SOLE NG
=======

This application is meant for sunbed control.


Hardware
--------

NXP LPC2106 ARM CPU based development board from Olimex - LPC-MT-2106.
See http://www.olimex.com/dev/lpc-mt.html.

Additionally a serial EEPROM was connected to the UEXT pins 5 (SCL) and 6 (SCA).

Software
--------

GNU/Linux cross compiling toolchain for ARMv7.
See https://sourcery.mentor.com/sgpp/lite/arm/portal/release2032.


Application
-----------

Main goal of the application is to mimic token insertions. Single token can be
seen as credit points defined a fixed value. Tokens are stored on the memory smart card. The
hardware simulates the tokens by openning/closing the relay contacts -
the sunbed as a single token.
