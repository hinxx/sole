TARGET			= sole-ng

# MCU name and submodel
MCU				= arm7tdmi-s
SUBMDL			= LPC2106

ifeq ($(MEMLOC),)
MEMLOC			= FLASH
endif
LDSCRIPT		= lpc2106-$(MEMLOC).ld

LOCATION		= /home/hinko.kocevar/Projects/ftmuzar/arm/tools/arm-2011.09
CC				= $(LOCATION)/bin/arm-none-eabi-gcc
LD				= $(LOCATION)/bin/arm-none-eabi-ld
AR				= $(LOCATION)/bin/arm-none-eabi-ar
AS				= $(LOCATION)/bin/arm-none-eabi-as
CP				= $(LOCATION)/bin/arm-none-eabi-objcopy
OD				= $(LOCATION)/bin/arm-none-eabi-objdump
SZ				= $(LOCATION)/bin/arm-none-eabi-size
STRIP			= $(LOCATION)/bin/arm-none-eabi-strip

# change the location of lpc21isp as appropriate
ISP				= /home/hinko.kocevar/Projects/ftmuzar/arm/tools/lpc21isp_183/lpc21isp

LIBS			= ./lib
SRCS			= ./src
CFLAGS			= -mcpu=$(MCU) -I$(SRCS) -I$(LIBS) -c -fno-common -O0
AFLAGS			= -mcpu=$(MCU) -ahls -mapcs-32
LDFLAGS			= -Map $(TARGET).map -T$(LDSCRIPT)
CPFLAGS			= -O ihex
ODFLAGS			= -x --syms
LIBRARIES		= 

LIBS_OBJS		= $(LIBS)/utils.o \
					$(LIBS)/events.o \
					$(LIBS)/delay.o \
					$(LIBS)/timer.o \
					$(LIBS)/button.o \
					$(LIBS)/buzzer.o \
					$(LIBS)/relay.o \
					$(LIBS)/lcd.o \
					$(LIBS)/led.o \
					$(LIBS)/uart0.o \
					$(LIBS)/uart1.o \
					$(LIBS)/sle4442.o \
					$(LIBS)/i2c.o \
					$(LIBS)/i2c_eeprom.o 

OBJS			= $(LIBS_OBJS) \
					$(SRCS)/system.o \
					$(SRCS)/interrupt.o \
					$(SRCS)/strings.o \
					$(SRCS)/fsm_super.o \
					$(SRCS)/fsm_user.o \
					$(SRCS)/main.o

all: info build

info:
	@echo "Code will run from $(MEMLOC)"
	@echo "using MCU = $(MCU)"
	@echo "using LDSCRIPT = $(LDSCRIPT)"

build: $(TARGET).hex list sizes test
	@echo "Size of $(TARGET).elf (stripped): $(shell stat -c "%s" $(TARGET).elf) bytes"
	@echo "Size of $(TARGET).hex: $(shell stat -c "%s" $(TARGET).hex) bytes"

clean:
	-rm -f *.o *.lst *.hex *.map *.elf *.lss *.dmp
	-rm -f $(OBJS)

test: $(TARGET).elf
	$(OD) $(ODFLAGS) $(TARGET).elf > $(TARGET).dmp

sizes: $(TARGET).elf
	$(SZ) $(TARGET).elf

list: $(TARGET).elf
	$(OD) $(ODFLAGS) $(TARGET).elf > $(TARGET).lss

program: $(TARGET).hex
# Use this if programmer can control ISP and RST LCP chip lines
#	$(ISP) -control $(TARGET).hex /dev/ttyS0 115200 14746

# Manual ISP and RST LPC chip lines control
	$(ISP) -verify $(TARGET).hex /dev/ttyUSB0 115200 14746

$(TARGET).elf: $(LDSCRIPT) crt.o $(OBJS) Makefile
	$(LD) $(LDFLAGS) -o $(TARGET).elf crt.o $(OBJS) $(LIBRARIES)

$(TARGET).hex: $(TARGET).elf
	$(STRIP) -s $(TARGET).elf
	$(CP) -O ihex $(TARGET).elf $(TARGET).hex

$(TARGET).srec: $(TARGET).elf
	$(CP) -O srec $(TARGET).elf $(TARGET).srec

$(TARGET).bin: $(TARGET).elf
	$(CP) -O binary $(TARGET).elf $(TARGET).bin

crt.o: crt.s
	$(AS) $(AFLAGS) crt.s -o crt.o > crt.lst

$(objects): %.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: build