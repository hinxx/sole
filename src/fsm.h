/*
 * fsm.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef FSM_H_
#define FSM_H_

#include <stdint.h>
#include <events.h>

/* default states */
#define ST_ANY				-1
#define ST_INIT				0
#define ST_ERROR			1
#define ST_TERM				2
#define ST_USER				3
#define ST_SUPER			4
/* new states */
#define ST_USER_SELECT		(ST_USER * 100 + 1)
#define ST_USER_PROCESS		(ST_USER * 100 + 2)
#define ST_USER_EXIT		(ST_USER * 100 + 3)

#define ST_SUPER_MENU		(ST_SUPER * 100 + 1)
#define ST_SUPER_VERSION	(ST_SUPER * 100 + 2)
#define ST_SUPER_TOK_TIME	(ST_SUPER * 100 + 3)
#define ST_SUPER_TOK_VAL	(ST_SUPER * 100 + 4)
#define ST_SUPER_EXIT		(ST_SUPER * 100 + 5)

/* default events are defined in events.h */
/* new events */
#define EV_SUPER_ENTER		(100 + 1)
#define EV_SUPER_LEAVE		(100 + 2)

#define EV_USER_ENTER		(200 + 1)
#define EV_USER_WAIT		(200 + 2)

#define EV_BUTTON_UP		(300 + 1)
#define EV_BUTTON_LEFT		(300 + 2)
#define EV_BUTTON_CENTER	(300 + 3)
#define EV_BUTTON_RIGHT		(300 + 4)
#define EV_BUTTON_DOWN		(300 + 5)

#define EV_CARD_INSERTED	(400 + 1)
#define EV_CARD_REMOVED		(400 + 2)
#define EV_CARD_OK			(400 + 3)
#define EV_CARD_ERROR		(400 + 4)
#define EV_CARD_UPDATE		(400 + 5)

/* LCD 16 character strings for super / user menu */
#define LCD_STR_CLR			"                "
//#define LCD_STR_UMENU		"* USER menu *"
//#define LCD_STR_UMENU2		"checking card"
//#define LCD_STR_UCARDREM	"remove card"
#define LCD_STR_UCARDERR	"card error"
#define LCD_STR_UCARDOK		"card OK"
#define LCD_STR_UCARDPIN	"card verified"
#define LCD_STR_UCARDSEL	"select time"
#define LCD_STR_UEMMEM		"MMEM"
#define LCD_STR_UEATR		"ATR"
#define LCD_STR_UEMAGIC		"MAGIC"
#define LCD_STR_UEPIN		"PIN"
#define LCD_STR_UEVALUE		"VALUE"

#define LCD_STR_SMENU		"* SUPER menu *"
#define LCD_STR_SMENU2		"use keys"
#define LCD_STR_SVER		"version"
#define LCD_STR_STIME		"token time"
#define LCD_STR_SVAL		"token value"
#define LCD_STR_SEXIT		"** EXIT **"
#define LCD_STR_SACC		"** ACCEPT **"

#define MSG_NAPAKA				"NAPAKA"
#define MSG_VSTAVI_KARTICO		"vstavi kartico"
#define MSG_ODSTRANI_KARTICO	"odstrani kartico"
#define MSG_POCAKAJ				"pocakaj.."

/* declarations for super.c */
int fsm_super_enter(void);
int fsm_super_menu(void);
int fsm_super_version(void);
int fsm_super_token_time(void);
int fsm_super_token_value(void);
int fsm_super_exit(void);
int fsm_super_leave(void);
int fsm_super_card_removed(void);

/* declarations for user.c */
int fsm_user_idle(void);
int fsm_user_enter(void);
int fsm_user_card_inserted(void);
int fsm_user_select(void);
int fsm_user_ok(void);
int fsm_user_card_ok(void);
int fsm_user_card_removed(void);
int fsm_user_card_error(void);
int fsm_user_card_update(void);
int fsm_user_wait(void);

#endif /* FSM_H_ */
