/*
 * card.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef CARD_H_
#define CARD_H_

#define CARD_MAGIC			6970

#define CARD_MEM_MAGIC		0x40
#define CARD_MEM_ID			0x44
#define CARD_MEM_TOTAL		0x48
#define CARD_MEM_VALUE		0x4C

#define card_int(x, o, v)	{ \
	unsigned char *__p = (unsigned char *)(x + o); \
	v = (unsigned int) (*(__p + 3) << 24) | \
		(*(__p + 2) << 16) | (*(__p + 1) << 8) | (*(__p)); \
}

/* handy macros for retrieving integer value from the memory card */
#define CARD_GET_MAGIC(x, v)	(card_int(x, CARD_MEM_MAGIC, v))
#define CARD_GET_ID(x, v)		(card_int(x, CARD_MEM_ID, v))
#define CARD_GET_TOTAL(x, v)	(card_int(x, CARD_MEM_TOTAL, v))
#define CARD_GET_VALUE(x, v)	(card_int(x, CARD_MEM_VALUE, v))

#endif /* CARD_H_ */
