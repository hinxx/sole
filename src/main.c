/*
 * main.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include <lpc210x_gnuarm.h>

#include "config.h"
#include "system.h"

#include <button.h>
#include <events.h>
#include <lcd.h>
#include <buzzer.h>
#include <delay.h>
#include <utils.h>
#include <led.h>
#include <timer.h>
#include <relay.h>
#include <uart0.h>
#include <sle4442.h>
#include <i2c_eeprom.h>

#include "fsm.h"
#include "card.h"
#include "strings.h"


/* extern in config.h */
const char *g_version = "soleng v0.4";
const char *author = "by Hinko";
const char *uart_hello = "\r\nsole NG 0.4 on LPC-MT-2106, by Hinxx\r\n";

static int fsm_init (void);
//static int do_idle (void);
static int fsm_error (void);

state_transition trans[] = {
		/* entry point states */
		{ ST_INIT,				EV_IDLE,				&fsm_init},
		{ ST_INIT,				EV_USER_ENTER,			&fsm_user_enter},

		/* USER states */
		{ ST_USER,				EV_IDLE,				&fsm_user_idle},
		{ ST_USER,				EV_CARD_INSERTED,		&fsm_user_card_inserted},
		{ ST_USER,				EV_SUPER_ENTER,			&fsm_super_enter},
		{ ST_USER,				EV_CARD_REMOVED,		&fsm_user_card_removed},
		{ ST_USER,				EV_CARD_ERROR,			&fsm_user_card_error},
		{ ST_USER,				EV_CARD_OK,				&fsm_user_ok},
		{ ST_USER,				EV_USER_WAIT,			&fsm_user_wait},

		{ ST_USER_SELECT,		EV_IDLE,				&fsm_user_select},

		{ ST_USER_PROCESS,		EV_IDLE,				&fsm_user_card_update},

		/* SUPER states */
		{ ST_SUPER,				EV_IDLE,				&fsm_super_menu},
		{ ST_SUPER,				EV_SUPER_LEAVE,			&fsm_super_leave},
		{ ST_SUPER,				EV_CARD_REMOVED,		&fsm_super_card_removed},

		{ ST_SUPER_MENU,		EV_IDLE,				&fsm_super_menu},

		{ ST_SUPER_VERSION,		EV_IDLE,				&fsm_super_version},

		{ ST_SUPER_TOK_TIME,	EV_IDLE,				&fsm_super_token_time},

		{ ST_SUPER_TOK_VAL,		EV_IDLE,				&fsm_super_token_value},

		{ ST_SUPER_EXIT,		EV_IDLE,				&fsm_super_exit},

		/* special CATCH ALL case - error in FSM */
		{ ST_ANY,			EV_ANY,						&fsm_error}
};
#define TRANS_COUNT			(sizeof(trans)/sizeof(*trans))

/* extern in config.h */
int g_token_time = 0;
int g_token_value = 0;


/* static function declarations */
static void __attribute__ ((interrupt("IRQ"))) defirqvect(void) { }

int main(void) {
	unsigned char byte = 0;

	/* initialize the system */
	system_init();

	/* clear all interrupts */
	VICIntEnClr = 0xFFFFFFFF;
	VICIntSelect = 0;
	/* point unvectored IRQs to reset() */
	VICDefVectAddr = (unsigned long int)defirqvect;

	/* initialize other subsystems */
	/* initialize UART0 */
	uart0_init(B115200, UART_8N1, UART_FIFO_8);
	/* initialize LED */
	led_init();
	/* initialize buttons */
	button_init();
	/* initialize buzzer */
	buzzer_init();
	/* initialize relay */
	relay_init();
	/* initialize LCD */
	lcd_init();
	/* initialize SLE4442 */
	sle4442_init();
	/* initialize I2C EEPROM (also initializes I2C) */
	i2c_eeprom_init();

	/* show some info */
	delay_ms(10);
	lcd_cursor_off();
	delay_ms(10);
	lcd_print_both(g_version, author);
	uart0_puts(uart_hello);
	delay_s(1);

	/* initialize our context */
	i2c_eeprom_read(0, &byte);
	g_token_time = byte;
	i2c_eeprom_read(1, &byte);
	g_token_value = byte;

	/* start the state machine */
	int state = ST_INIT;
	int event = EV_ANY;
	int i;
	while (state != ST_TERM) {

		event = events_get_next();

		if ((state != ST_USER) && (event != EV_IDLE)) {
			uart0_puts("main(): state = ");
			uart0_puts(state_str(state));
			uart0_puts(", event = ");
			uart0_puts(event_str(event));
			uart0_puts("\r\n");
		}

		led_on();

		for (i = 0; i < TRANS_COUNT; i++) {
			if ((state == trans[i].st) || (ST_ANY == trans[i].st)) {
				if ((event == trans[i].ev) || (EV_ANY == trans[i].ev)) {
					state = (trans[i].fn)();
					break;
				}
			}
		}

		led_off();

		// tweak the sleep to your needs
		delay_ms(300);
		uart0_putc('.');
	}

	uart0_puts("\r\nBye from LPC-MT-2106!\r\n");
	return 0;
}

static int fsm_init(void) {

	/* do something to (re-)initialize.. */
	uart0_puts("fsm_init()\r\n");

	/* will enter USER state when handled */
	events_set_next(EV_USER_ENTER);

	/* remain in init state */
	return ST_INIT;
}

static int fsm_error(void) {

	uart0_puts("fsm_error()\r\n");
	/* terminate the program */
	return ST_TERM;
}
