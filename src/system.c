/*
 * system.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "config.h"
#include "system.h"

#define PLOCK			0x400

void feed(void);

void system_init(void) {

	/*
	 * 				Setting the Phased Lock Loop (PLL)
	 *               ----------------------------------
	 *
	 * Olimex LPC-P2106 has a 14.7456 mhz crystal
	 *
	 * We'd like the LPC2106 to run at 53.2368 mhz (has to be an even
	 * multiple of crystal)
	 * According to the Philips LPC2106 manual:
	 *   M = cclk / Fosc
	 *   where:	M    = PLL multiplier (bits 0-4 of PLLCFG)
	 *			cclk = 53236800 hz
	 *			Fosc = 14745600 hz
	 *
	 * Solving:	M = 53236800 / 14745600 = 3.6103515625
	 *			M = 4 (round up)
	 *
	 * Note: M - 1 must be entered into bits 0-4 of PLLCFG
	 *       (assign 3 to these bits)
	 *
	 * The Current Controlled Oscilator (CCO) must operate in the
	 * range 156 mhz to 320 mhz
	 * According to the Philips LPC2106 manual:
	 *   Fcco = cclk * 2 * P
	 *   where:	Fcco = CCO frequency
	 *			cclk = 53236800 hz
	 *			P = PLL divisor (bits 5-6 of PLLCFG)
	 *
	 * Solving:	Fcco = 53236800 * 2 * P
	 *			P = 2  (trial value)
	 *			Fcco = 53236800 * 2 * 2
	 *			Fcc0 = 212947200 hz
	 *
	 * Note: 212947200 hz is good choice for P since it's within the
	 *       156 mhz to 320 mhz range
	 *
	 * From Table 19 (page 48) of Philips LPC2106 manual
	 *   P = 2, PLLCFG bits 5-6 = 1  (assign 1 to these bits)
	 *
	 * Finally:
	 *   PLLCFG = 0  01  00011  =  0x23
	 *
	 * Final note: to load PLLCFG register, we must use the 0xAA followed
	 *             0x55 write sequence to the PLLFEED register
	 *             this is done in the short function feed() below
	 */

	/* Setting Multiplier and Divider values */
	SCB_PLLCFG = 0x23;
	feed();

	/* Enabling the PLL */
	SCB_PLLCON = 0x1;
	feed();

	/* Wait for the PLL to lock to set frequency */
	while (!(SCB_PLLSTAT & PLOCK))
		;

	/* Connect the PLL as the clock source */
	SCB_PLLCON = 0x3;
	feed();

	/* --- setup and enable the MAM (Memory Accelerator Module) --- */
	/* a. start change by turning of the MAM (redundant) */
	MAM_MAMCR = 0;
	/* b. set MAM-Fetch cycle to 3 cclk as recommended for >40MHz */
	MAM_MAMTIM = 3;
	/* c. enable MAM */
	MAM_MAMCR = 2;

	/* Setting peripheral Clock (pclk) to System Clock (cclk) */
	SCB_VPBDIV = VPBDIV_VAL;

	/* initialize PINSEL0 & PINSEL1 */
	PCB_PINSEL0 = 0x00;
	PCB_PINSEL1 = 0x00;
}

void feed(void) {
	SCB_PLLFEED = 0xAA;
	SCB_PLLFEED = 0x55;
}
