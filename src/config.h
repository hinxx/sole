/*
 * config.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef CONFIG_H_
#define CONFIG_H_

/*
 * board / hardware specific definitions
 */

/* oscillator clock is at 14,7456 MHz */
#define F_OSC			14745600

/* PLL multiplier - system should run frequency: PLL_M * F_CPU */
/* NOTE: 60MHz is max */
#define PLL_M			4	/* 4 * 14.7 MHz ~= 59 MHz */

/* APB should run at full speed, divider 1 (=> pclk = cclk = 59MHz) */
#define VPBDIV_VAL		1

/*
 * application specific definitions and shared variables
 */
/* application version string */
extern const char *g_version;
/* token time in minutes */
extern int g_token_time;
/* token value in EUR */
extern int g_token_value;

/* Debug control is possible over separate modules */
//#define DEBUG_I2C
//#define DEBUG_EEPROM
//#define DEBUG_SLE4442
//#define DEBUG_USER
//#define DEBUG_SUPER

#endif /* CONFIG_H_ */
