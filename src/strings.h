/*
 * strings.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef STRINGS_H_
#define STRINGS_H_

#include <stdint.h>
#include <events.h>
#include "fsm.h"

typedef struct {
	uint32_t id;
	char *txt;
} states_t;

typedef struct {
	uint32_t id;
	char *txt;
} events_t;

const char *state_str(int32_t id);
const char *event_str(int32_t id);

#endif /* STRINGS_H_ */
