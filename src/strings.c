/*
 * strings.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "strings.h"

#define UNKNOWN		"???"

states_t states[] = {
		{ .id = ST_ANY,				.txt = "ANY" },
		{ .id = ST_INIT,			.txt = "INIT" },
		{ .id = ST_ERROR,			.txt = "ERROR" },
		{ .id = ST_TERM,			.txt = "TERM" },

		{ .id = ST_USER,			.txt = "USER" },
		{ .id = ST_USER_SELECT,		.txt = "USER_SELECT" },
		{ .id = ST_USER_PROCESS,	.txt = "USER_PROCESS" },
		{ .id = ST_USER_EXIT,		.txt = "USER_EXIT" },

		{ .id = ST_SUPER,			.txt = "SUPER" },
		{ .id = ST_SUPER_MENU,		.txt = "SUPER_MENU" },
		{ .id = ST_SUPER_VERSION,	.txt = "SUPER_VERSION" },
		{ .id = ST_SUPER_TOK_TIME,	.txt = "SUPER_TOK_TIME" },
		{ .id = ST_SUPER_TOK_VAL,	.txt = "SUPER_TOK_VAL" },
		{ .id = ST_SUPER_EXIT,		.txt = "SUPER_EXIT" },
};
#define STATES_COUNT			(sizeof(states)/sizeof(*states))

events_t events[] = {
		{ .id = EV_ANY,				.txt = "ANY" },
		{ .id = EV_IDLE,			.txt = "IDLE" },
		{ .id = EV_USER_ENTER,		.txt = "USER_ENTER" },
		{ .id = EV_SUPER_ENTER,		.txt = "SUPER_ENTER" },
		{ .id = EV_SUPER_LEAVE,		.txt = "SUPER_LEAVE" },
		{ .id = EV_BUTTON_UP,		.txt = "BUTTON_UP" },
		{ .id = EV_BUTTON_LEFT,		.txt = "BUTTON_LEFT" },
		{ .id = EV_BUTTON_CENTER,	.txt = "BUTTON_CENTER" },
		{ .id = EV_BUTTON_RIGHT,	.txt = "BUTTON_RIGTH" },
		{ .id = EV_BUTTON_DOWN,		.txt = "BUTTON_DOWN" },
		{ .id = EV_CARD_INSERTED,	.txt = "CARD_INSERTED" },
		{ .id = EV_CARD_REMOVED,	.txt = "CARD_REMOVED" },
		{ .id = EV_CARD_OK,			.txt = "CARD_OK" },
		{ .id = EV_CARD_ERROR,		.txt = "CARD_ERROR" },
		{ .id = EV_CARD_UPDATE,		.txt = "CARD_UPDATE" },
};
#define EVENTS_COUNT			(sizeof(events)/sizeof(*events))

const char *state_str(int32_t id) {
	int i;
	for (i = 0; i < STATES_COUNT; i++) {
		if (id == states[i].id) {
			return states[i].txt;
		}
	}
	return UNKNOWN;
}

const char *event_str(int32_t id) {
	int i;
	for (i = 0; i < EVENTS_COUNT; i++) {
		if (id == events[i].id) {
			return events[i].txt;
		}
	}
	return UNKNOWN;
}
