/*
 * fsm_user.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "config.h"
#include "system.h"

#include <button.h>
#include <events.h>
#include <lcd.h>
#include <sle4442.h>
#include <buzzer.h>
#include <delay.h>
#include <utils.h>
#include <debug.h>

#include "fsm.h"
#include "card.h"

//static uint8_t card_mmem_buf[256] = {0};
//static uint8_t card_smem_buf[4] = {0};
//static uint8_t hex_buf[1024] = {0};
/* LCD data scratch buffer */
//static char lcd_buffer[40];
static int user_value = 0;
static int user_time = 0;
static int token_count = 0;
static int card_tokens = 0;
static int card_value = 0;
static int card_id = 0;
static int card_total = 0;
static int card_magic = 0;
static char *err_msg = "";
static int wait_time = 0;

int fsm_user_enter(void) {

	D_USERNL("enter");

	/* menu */
	lcd_print_both(g_version, MSG_VSTAVI_KARTICO);

	/* get rid of events */
	events_reset();

	/* enter user state */
	return ST_USER;
}

int fsm_user_idle(void) {

	/* see if card is present */
	if (sle4442_detect()) {
		buzzer_beep();
		events_set_next(EV_CARD_INSERTED);
	}

	/* remain in USER state */
	return ST_USER;
}

int fsm_user_card_inserted(void) {
	char card_mmem_buf[300] = {0};
	char card_smem_buf[4] = {0};

	D_USERNL("enter");

	/* card inserted */
	lcd_print_both(g_version, MSG_POCAKAJ);

	/* perform card reset */
	if (sle4442_reset() == -1) {
		/* card reset failed */
		err_msg = LCD_STR_UEATR;
		D_USERNL("ATR error!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_ERROR);
		return ST_USER;
	}

	/* card reset OK */
	D_USERNL("ATR OK!");

	if (sle4442_read(card_mmem_buf, card_smem_buf) == -1) {
		/* card read failed */
		err_msg = LCD_STR_UEMMEM;
		D_USERNL("MMEM read error!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_ERROR);
		return ST_USER;
	}

	/* card read OK */
	D_USERNL("MMEM read OK!");

	/* get card info */
	CARD_GET_MAGIC(card_mmem_buf, card_magic);
	CARD_GET_TOTAL(card_mmem_buf, card_total);
	CARD_GET_ID(card_mmem_buf, card_id);
	CARD_GET_VALUE(card_mmem_buf, card_value);

#ifdef DEBUG_USER
	unsigned char s = 0;
	unsigned char hex_buf[1024] = {0};
	uart0_puts("CARD MAGIC: ");
	sprintf_i(hex_buf, card_magic);
	uart0_puts(hex_buf);
	uart0_puts("\r\n");

	uart0_puts("CARD    ID: ");
	sprintf_i(hex_buf, card_id);
	uart0_puts(hex_buf);
	uart0_puts("\r\n");

	uart0_puts("CARD TOTAL: ");
	sprintf_i(hex_buf, card_total);
	uart0_puts(hex_buf);
	uart0_puts("\r\n");

	uart0_puts("CARD VALUE: ");
	sprintf_i(hex_buf, card_value);
	uart0_puts(hex_buf);
	uart0_puts("\r\n");
#endif

	if (card_magic != CARD_MAGIC) {
		/* card magic check failed */
		err_msg = LCD_STR_UEMAGIC;
		D_USERNL("MAGIC error!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_ERROR);
		return ST_USER;
	}

	unsigned char pin[3] = {0xC0, 0xDE, 0xA5};
	if (sle4442_verify_pin(pin) == -1) {
		/* card verify failed */
		err_msg = LCD_STR_UEPIN;
		D_USERNL("PIN verify error!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_ERROR);
		return ST_USER;
	}

	/* card verify OK */
	D_USERNL("PIN verify OK!");

	if (card_value < g_token_value) {
		/* card has insufficient value left */
		err_msg = LCD_STR_UEVALUE;
		D_USERNL("VALUE error!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_ERROR);
		return ST_USER;
	}

	/* card looks sane, initialize session */
	buzzer_short_beep();

	/* initial value and time */
	user_value = 0;
	user_time = 0;
	token_count = 0;
	card_tokens = 0;

	/* do we have money for at least one session? */
	if (card_value >= g_token_value) {
		token_count = 1;
		user_value = g_token_value;
		user_time = g_token_time;
		{
			int tmp = card_value;
			while (tmp > g_token_value) {
				card_tokens++;
				tmp -= g_token_value;
			}
		}
	}

	D_USERNL("card OK!");
	if (card_id == 1) {
		/* ID == 1 will enter super state */
		events_set_next(EV_SUPER_ENTER);
		/* remain in current state, wait for event handle */
		return ST_USER;
	} else {
		/* go to next user state */
		return ST_USER_SELECT;
	}
}

int fsm_user_select(void) {
	char lcd_buffer[40] = {0};

	D_USERNL("enter");

	if (! sle4442_detect()) {
		D_USERNL("card removed!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_REMOVED);
		return ST_USER;
	}

	buttons *btns = button_states();
	if (btns->up) {
		/* increment user value */
		token_count++;
		if (token_count > 20 || (token_count > card_tokens)) {
			token_count--;
		}
		user_value = g_token_value * token_count;
		user_time = g_token_time * token_count;

	} else if (btns->left) {
	} else if (btns->center) {
		/* selecting user value */
		buzzer_short_beep();
		delay_ms(300);
		/* selected, go to next state  */
		return ST_USER_PROCESS;

	} else if (btns->right) {
	} else if (btns->down) {
		/* decrement user value */
		token_count--;
		if (token_count < 1) {
			token_count++;
		}
		user_value = g_token_value * token_count;
		user_time = g_token_time * token_count;
	}

	sprintf_sis(lcd_buffer, "vrednost ", card_value, " EUR");
	lcd_print_line1(lcd_buffer);
	sprintf_isis(lcd_buffer, user_value, " EUR / ", user_time, " min");
	lcd_print_line2(lcd_buffer);

	/* remain in current state, wait for select */
	return ST_USER_SELECT;
}

int fsm_user_card_update(void) {
	char card_mmem_buf[4] = {0};
	char lcd_buffer[40] = {0};

	D_USERNL("enter");

	if (! sle4442_detect()) {
		D_USERNL("card removed!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_REMOVED);
		return ST_USER;
	}

	buttons *btns = button_states();

	if (btns->up) {
	} else if (btns->left) {
	} else if (btns->center) {
		/* setting user value */
		lcd_print_both("potrdi izbiro", "** sprejeto **");

		buzzer_short_beep();

		/* update card value */
		int value = card_value - user_value;
		card_mmem_buf[0] = (unsigned char)((value) & 0xFF);
		card_mmem_buf[1] = (unsigned char)((value >> 8) & 0xFF);
		card_mmem_buf[2] = (unsigned char)((value >> 16) & 0xFF);
		card_mmem_buf[3] = (unsigned char)((value >> 24) & 0xFF);
		if (sle4442_update_block(CARD_MEM_VALUE, card_mmem_buf, 4) == -1) {
			/* card write failed */
			lcd_print_both(LCD_STR_UCARDERR, LCD_STR_UEMMEM);
			D_USERNL("MMEM write error!");
			buzzer_long_beep();
			/* bail out with error */
			events_set_next(EV_CARD_ERROR);
			return ST_USER;
		}

		lcd_print_line1("hvala na");
		lcd_print_line2("uporabi! :)");
		delay_s(1);

		/* toggle relay */
		relay_open();
		delay_ms(10);
		while (token_count--) {
			lcd_print_both(LCD_STR_CLR, LCD_STR_CLR);
			delay_ms(500);
			relay_close();
			led_on();
			buzzer_short_beep();
			D_USERNL("token inserted");
			lcd_print_line1("hvala na");
			lcd_print_line2("uporabi! :)");
			delay_ms(500);
			relay_open();
			led_off();
		}

		/* transaction complete, next state .. go home */
		events_set_next(EV_CARD_OK);
		return ST_USER;

	} else if (btns->right) {
	} else if (btns->down) {
	}

	lcd_print_line1("potrdi izbiro");
	sprintf_isis(lcd_buffer, user_value, " EUR / ", user_time, " min");
	lcd_print_line2(lcd_buffer);

	/* remain in current state, wait for confirmation */
	return ST_USER_PROCESS;
}

int fsm_user_ok(void) {

	D_USERNL("enter");

	lcd_print_line1("solarij je");
	lcd_print_line2("pripravljen");

	delay_s(1);

	/* see if card is present */
	if (sle4442_detect()) {
		lcd_print_line1("odstrani");
		lcd_print_line2("kartico");
		D_USERNL("card still present!");
		buzzer_long_beep();
		delay_s(1);
		/* wait until user removes the card */
		events_set_next(EV_CARD_OK);
	} else {
		D_USERNL("card removed!");
		/* wait until tanning finishes, include power on/off time */
		wait_time = (user_time + 4 + 4) * 60;
		events_set_next(EV_USER_WAIT);
	}

	/* remain in current state */
	return ST_USER;
}

int fsm_user_wait(void) {
	char lcd_buffer[40] = {0};

	D_USERNL("enter");

	lcd_print_line1("zasedeno se");
	sprintf_is(lcd_buffer, (wait_time / 60), " min");
	lcd_print_line2(lcd_buffer);

	delay_s(1);

	/* wait .. */
	if (wait_time > 60) {
		lcd_print_line1("pocakajte");
		lcd_print_line2("prosim");
		delay_s(1);
		/* wait until tanning is done */
		events_set_next(EV_USER_WAIT);
	} else {
		D_USERNL("waiting expired!");
		buzzer_short_beep();
		delay_s(1);
		buzzer_short_beep();
		delay_s(1);
		/* go home */
		return fsm_user_enter();
	}

	/* two seconds have passed */
	wait_time -= 2;

	/* remain in current state */
	return ST_USER;
}

int fsm_user_card_error(void) {

	D_USERNL("enter");

	lcd_print_both(MSG_NAPAKA, err_msg);

	delay_s(1);

	/* see if card is present */
	if (sle4442_detect()) {
		lcd_print_line1("odstrani");
		lcd_print_line2("kartico");
		D_USERNL("card still present!");
		buzzer_long_beep();
		delay_s(1);
		/* wait until user removes the card */
		events_set_next(EV_CARD_ERROR);
	} else {
		D_USERNL("card removed!");
		/* go home */
		return fsm_user_enter();
	}

	/* remain in current state */
	return ST_USER;
}

int fsm_user_card_removed(void) {

	D_USERNL("enter");

	lcd_print_both(MSG_NAPAKA, "NO CARD");

	delay_s(1);

	return fsm_user_enter();
}
