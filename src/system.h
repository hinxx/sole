/*
 * setup.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef SETUP_H_
#define SETUP_H_

void system_init(void);

#endif /* SETUP_H_ */
