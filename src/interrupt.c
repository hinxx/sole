/*
 * interrupt.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "config.h"
#include "interrupt.h"

void IRQ_Routine(void) {
	while (1)
		;
}

void FIQ_Routine(void) {
	while (1)
		;
}

void SWI_Routine(void) {
	while (1)
		;
}

void UNDEF_Routine(void) {
	while (1)
		;
}

static inline unsigned asm_get_cpsr(void) {
	unsigned long retval;
	__asm__ __volatile__ (" mrs  %0, cpsr" : "=r" (retval) : /* no inputs */);
	return retval;
}

static inline void asm_set_cpsr(unsigned val) {
	__asm__ __volatile__ (" msr  cpsr, %0" : /* no outputs */: "r" (val) );
}

unsigned irq_enable(void) {
	unsigned _cpsr;

	_cpsr = asm_get_cpsr();
	asm_set_cpsr(_cpsr & ~IRQ_MASK);
	return _cpsr;
}

unsigned irq_disable(void) {
	unsigned _cpsr;

	_cpsr = asm_get_cpsr();
	asm_set_cpsr(_cpsr | IRQ_MASK);
	return _cpsr;
}

unsigned irq_restore(unsigned oldCPSR) {
	unsigned _cpsr;

	_cpsr = asm_get_cpsr();
	asm_set_cpsr((_cpsr & ~IRQ_MASK) | (oldCPSR & IRQ_MASK));
	return _cpsr;
}
