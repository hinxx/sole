/*
 * interrupt.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef INTERRUPT_H_
#define INTERRUPT_H_

void IRQ_Routine(void) __attribute__ ((interrupt("IRQ")));
void FIQ_Routine(void) __attribute__ ((interrupt("FIQ")));
void SWI_Routine(void) __attribute__ ((interrupt("SWI")));
void UNDEF_Routine(void) __attribute__ ((interrupt("UNDEF")));

#define IRQ_MASK		0x00000080

unsigned irq_enable(void);
unsigned irq_disable(void);
unsigned irq_restore(unsigned oldCPSR);

#endif /* INTERRUPT_H_ */
