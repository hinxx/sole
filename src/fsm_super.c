/*
 * fsm_super.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "config.h"
#include "system.h"

#include <button.h>
#include <events.h>
#include <lcd.h>
#include <buzzer.h>
#include <delay.h>
#include <utils.h>
#include <debug.h>
#include <i2c_eeprom.h>

#include "fsm.h"

/* used for token time and value manipulation */
static int token_time = 0;
static int token_value = 0;
/* LCD data scratch buffer */
static char lcd_buffer[20];

int fsm_super_enter(void) {

	D_SUPERNL("enter");

	/* show menu */
	lcd_print_both(LCD_STR_SMENU, LCD_STR_SMENU2);

	/* get rid of events */
	events_reset();

	/* reset values for manipulation */
	token_time = g_token_time;
	token_value = g_token_value;

	/* enter next state */
	return ST_SUPER_MENU;
}

int fsm_super_menu(void) {

	D_SUPERNL("enter");

	if (! sle4442_detect()) {
		D_SUPERNL("card removed!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_REMOVED);
		return ST_SUPER;
	}

	buttons *btns = button_states();

	if (btns->up) {
	} else if (btns->left) {
		lcd_print_both(LCD_STR_SEXIT, LCD_STR_SEXIT);
		/* enter state on the left */
		return ST_SUPER_EXIT;
	} else if (btns->center) {
	} else if (btns->right) {
		lcd_print_both(LCD_STR_SVER, g_version);
		/* enter state on the right */
		return ST_SUPER_VERSION;
	} else if (btns->down) {
	}

	/* remain in current state */
	return ST_SUPER_MENU;
}

int fsm_super_version(void) {

	D_SUPERNL("enter");

	if (! sle4442_detect()) {
		D_SUPERNL("card removed!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_REMOVED);
		return ST_SUPER;
	}

	buttons *btns = button_states();

	if (btns->up) {
	} else if (btns->left) {
		lcd_print_both(LCD_STR_SMENU, LCD_STR_SMENU2);
		/* enter state on the left */
		return ST_SUPER_MENU;
	} else if (btns->center) {
	} else if (btns->right) {
		sprintf_is(lcd_buffer, token_time, " min");
		lcd_print_both(LCD_STR_STIME, lcd_buffer);
		/* enter state on the right */
		return ST_SUPER_TOK_TIME;
	} else if (btns->down) {
	}

	/* remain in current state */
	return ST_SUPER_VERSION;
}

int fsm_super_token_time(void) {

	D_SUPERNL("enter");

	if (! sle4442_detect()) {
		D_SUPERNL("card removed!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_REMOVED);
		return ST_SUPER;
	}

	buttons *btns = button_states();

	if (btns->up) {
		/* token time */
		token_time++;
		if (token_time >= 20) {
			token_time = 20;
		}
		sprintf_is(lcd_buffer, token_time, " min");
		lcd_print_both(LCD_STR_STIME, lcd_buffer);

	} else if (btns->left) {
		lcd_print_both(LCD_STR_SVER, g_version);
		/* enter state on the left */
		return ST_SUPER_VERSION;
	} else if (btns->center) {
		/* setting token time */
		lcd_print_both(LCD_STR_SVAL, LCD_STR_SACC);

		g_token_time = token_time;
		i2c_eeprom_write(0, (unsigned char)g_token_time);

		buzzer_beep();
		delay_s(1);

		sprintf_is(lcd_buffer, token_time, " min");
		lcd_print_line2(lcd_buffer);
	} else if (btns->right) {
		sprintf_is(lcd_buffer, token_value, " EUR");
		lcd_print_both(LCD_STR_SVAL, lcd_buffer);
		/* enter state on the right */
		return ST_SUPER_TOK_VAL;
	} else if (btns->down) {
		/* token time */
		token_time--;
		if (token_time <= 2) {
			token_time = 2;
		}
		sprintf_is(lcd_buffer, token_time, " min");
		lcd_print_both(LCD_STR_STIME, lcd_buffer);
	}

	/* remain in current state */
	return ST_SUPER_TOK_TIME;
}

int fsm_super_token_value(void) {

	D_SUPERNL("enter");

	if (! sle4442_detect()) {
		D_SUPERNL("card removed!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_REMOVED);
		return ST_SUPER;
	}

	buttons *btns = button_states();

	if (btns->up) {
		/* token value */
		token_value++;
		if (token_value >= 20) {
			token_value = 20;
		}
		sprintf_is(lcd_buffer, token_value, " EUR");
		lcd_print_both(LCD_STR_SVAL, lcd_buffer);

	} else if (btns->left) {
		sprintf_is(lcd_buffer, token_time, " min");
		lcd_print_both(LCD_STR_STIME, lcd_buffer);
		/* enter state on the left */
		return ST_SUPER_TOK_TIME;
	} else if (btns->center) {
		/* setting token value */
		lcd_print_both(LCD_STR_SVAL, LCD_STR_SACC);

		g_token_value = token_value;
		i2c_eeprom_write(1, (unsigned char)g_token_value);

		buzzer_beep();
		delay_s(1);

		sprintf_is(lcd_buffer, token_value, " EUR");
		lcd_print_line2(lcd_buffer);
	} else if (btns->right) {
		lcd_print_both(LCD_STR_SEXIT, LCD_STR_SEXIT);
		/* enter state on the right */
		return ST_SUPER_EXIT;
	} else if (btns->down) {
		/* token value */
		token_value--;
		if (token_value <= 2) {
			token_value = 2;
		}
		sprintf_is(lcd_buffer, token_value, " EUR");
		lcd_print_both(LCD_STR_SVAL, lcd_buffer);
	}

	/* remain in current state */
	return ST_SUPER_TOK_VAL;
}

int fsm_super_exit(void) {

	D_SUPERNL("enter");

	if (! sle4442_detect()) {
		D_SUPERNL("card removed!");
		buzzer_long_beep();
		/* bail out with error */
		events_set_next(EV_CARD_REMOVED);
		return ST_SUPER;
	}

	buttons *btns = button_states();

	if (btns->up) {

	} else if (btns->left) {
		sprintf_is(lcd_buffer, token_value, " EUR");
		lcd_print_both(LCD_STR_SVAL, lcd_buffer);
		/* enter state on the left */
		return ST_SUPER_TOK_VAL;
	} else if (btns->center) {
		buzzer_beep();
		/* leaving super state */
		events_set_next(EV_SUPER_LEAVE);
		return ST_SUPER;
	} else if (btns->right) {
		lcd_print_both(LCD_STR_SMENU, LCD_STR_SMENU2);
		/* enter state on the right */
		return ST_SUPER_MENU;
	} else if (btns->down) {

	}

	/* remain in current state */
	return ST_SUPER_EXIT;
}

int fsm_super_leave(void) {

	D_SUPERNL("enter");

	lcd_print_line1(LCD_STR_CLR);
	lcd_print_line2(LCD_STR_CLR);

	delay_s(1);

	/* see if card is present */
	if (sle4442_detect()) {
		lcd_print_line1("odstrani");
		lcd_print_line2("kartico");
		D_SUPERNL("card still present!");
		buzzer_long_beep();
		delay_s(1);
		/* wait until user removes the card */
		events_set_next(EV_SUPER_LEAVE);
	} else {
		D_SUPERNL("card removed!");
		/* go to user menu */
		return fsm_user_enter();
	}

	/* remain in current state */
	return ST_SUPER;
}

int fsm_super_card_removed(void) {

	D_SUPERNL("enter");

	lcd_print_both(MSG_NAPAKA, "NO CARD");

	delay_s(1);

	return fsm_user_enter();
}
