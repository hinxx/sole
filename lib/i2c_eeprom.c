/*
 * i2c_eeprom.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "i2c.h"
#include "i2c_eeprom.h"
#include "utils.h"
#include "debug.h"

/* ST24c02 EEPROM default address is 1 0 1 0 E2 E1 E0 R/W
 * in our design (Olimex LPC-MT-2106):
 *  E0, E1, and E2 are grounded
 *  MODE/~WC is grounded
 *  SCL line is additionally pulled high with 4k7 resistor */

#define EEPROM_ADDRESS			0xA0
static i2cdev_t eeprom_dev;

void i2c_eeprom_init() {

	/* LPC2106 data sheet
	 * Table 94: I2C Clock Rate Selections for VPB Clock Divider = 1
	 * 800 = I2SCLL + I2SCLH == 75 kHz
	 */
	i2c_init(400, 400);
	D_EEPROMNL("initialized!");
}

int i2c_eeprom_write(unsigned char offset, unsigned char data) {
	D_EEPROMNL("sending SLA & data byte");

	memset(eeprom_dev.data, 0, sizeof(eeprom_dev.data));
	eeprom_dev.data[0] = EEPROM_ADDRESS;
	eeprom_dev.data[1] = offset;
	eeprom_dev.data[2] = data;
	eeprom_dev.len = 3;
	eeprom_dev.cnt = 0;
	int ret = 0;

	ret = i2c_master_send(&eeprom_dev);
	if (ret != 0) {
		D_EEPROMNL("EEPROM SLA / data error");
		return ret;
	}

	return 0;
}

int i2c_eeprom_read(unsigned char offset, unsigned char *data) {
	D_EEPROMNL("sending SLA");

	memset(eeprom_dev.data, 0, sizeof(eeprom_dev.data));
	eeprom_dev.data[0] = EEPROM_ADDRESS;
	eeprom_dev.data[1] = offset;
	eeprom_dev.len = 2;
	eeprom_dev.cnt = 0;
	int ret = 0;

	ret = i2c_master_send(&eeprom_dev);
	if (ret != 0) {
		D_EEPROMNL("EEPROM SLA error");
		return ret;
	}

	D_EEPROMNL("receiving data byte");
	memset(eeprom_dev.data, 0, sizeof(eeprom_dev.data));
	eeprom_dev.data[0] = EEPROM_ADDRESS | 1;
	eeprom_dev.len = 1;
	eeprom_dev.cnt = 0;

	ret = i2c_master_recv(&eeprom_dev);
	if (ret != 0) {
		D_EEPROMNL("EEPROM data error");
		return ret;
	}

#ifdef DEBUG_EEPROM
	char hex_buf[48] = {0};
	D_EEPROM("EEPROM data[16]: ");
	sprintf_hex(hex_buf, eeprom_dev.data, 16);
	uart0_puts(hex_buf);
	uart0_puts("\r\n");
#endif

	/* first byte is device address */
	*data = eeprom_dev.data[1];

	return ret;
}
