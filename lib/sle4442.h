/*
 * sle4442.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef SLE4442_H_
#define SLE4442_H_

#include <stdint.h>

#define SLE4442_RST_PIN			16
#define SLE4442_IO_PIN			26
#define SLE4442_DET_PIN			25
#define SLE4442_CLK_PIN			15

#define SLE4442_RST				(1 << SLE4442_RST_PIN)
#define SLE4442_IO				(1 << SLE4442_IO_PIN)
#define SLE4442_DET				(1 << SLE4442_DET_PIN)
#define SLE4442_CLK				(1 << SLE4442_CLK_PIN)

/* SLE4442 memory card commands and responses */

/* Length (in bits) of ATR response and command */
#define SLE4442_ATR_LEN			4
#define SLE4442_CMD_LEN			3

/* SLE4442 256b memory sizes:
 *  Main memory is 256 bytes,
 *  protection and security memory are 4 bytes each */
#define SLE4442_MMEM_SZ			256
#define SLE4442_PMEM_SZ			4
#define SLE4442_SMEM_SZ			4

/* SLE4442 memory card read command codes */
#define SLE4442_RD_MMEM			0x30
#define	SLE4442_RD_PMEM			0x34
#define SLE4442_RD_SMEM			0x31

/* SLE4442 memory card update command codes */
#define SLE4442_UP_MMEM			0x38
#define SLE4442_UP_PMEM			0x3C
#define SLE4442_UP_SMEM			0x39
#define SLE4442_CMP_DATA		0x33

/* Default size for command buffer (always 3 bytes!) and data buffer (can vary
 * from 1 - 256 bytes) */
#define SLE4442_CMD_BUF			3
#define SLE4442_DATA_BUF		256

/* EC value tells us how many PIN verification tries are left
 *  EC0-7: 0, 1, 2, 3 tries left
 */
#define SLE4442_PSC_EC0			0x00
#define SLE4442_PSC_EC1			0x01
#define SLE4442_PSC_EC3			0x03
#define SLE4442_PSC_EC7			0x07
#define SLE4442_PSC_MASK		0x07
#define SLE4442_PSC_ERR			0xFF


void sle4442_init(void);
int32_t sle4442_detect(void);
int32_t sle4442_reset(void);
int32_t sle4442_read(uint8_t *mmem, uint8_t *smem);
int32_t sle4442_update_block(uint32_t address, uint8_t *mmem, uint32_t len);
int32_t sle4442_verify_pin(uint8_t *pin);

#endif /* SLE4442_H_ */
