/*
 * delay.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef DELAY_H_
#define DELAY_H_

void delay_us(int count);
void delay_ms(int count);
void delay_s(int count);

#endif /* DELAY_H_ */
