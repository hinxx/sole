/*
 * uart1.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef UART1_H_
#define UART1_H_

#include "lpc210x_gnuarm.h"
#include "config.h"
#include "uart.h"


/* UART1 TX-Pin=P0.8, RX-Pin=P0.9
 * PINSEL0 has to be set to "UART-Function" = Function "01" for Pin 0.8 and 0.9
 */

#define PINSEL_TXD1			16
#define PINSEL_RXD1			18

/* PINSEL0 bits to activate UART1 */
#define UART1_PINSEL		((0x01 << PINSEL_TXD1) | (0x01 << PINSEL_RXD1))
/* UART1 PINSEL0 mask */
#define UART1_PINMASK		(0x000F0000)

/* UART1 interrupt enable bit in VIC */
#define UART1_IntEnBit		7

/* UART1 */
void uart1_init(uint16_t baud, uint8_t mode, uint8_t fmode);
int uart1_putc(int ch);
uint16_t uart1_space(void);
const char *uart1_puts(const char *str);
int uart1_tx_empty(void);
void uart1_tx_flush(void);
int uart1_getc(void);

void uart1_set_handler(unsigned long *ptr);
#if 0
void uart1_tx_send();
#endif

#endif /* UART1_H_ */
