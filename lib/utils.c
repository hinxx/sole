/*
 * utils.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "utils.h"


int strlen(char *s) {
	int c = 0;
	while (*s++ != 0) { c++; }
	return c;
}

void *memset(void *s, int c, uint32_t n) {
	char *p = (char *)s;
	while (n--) { *p++ = c; }
	return s;
}

void *memcpy(void *dest, const void *src, uint32_t n) {
	char *s = (char *)src;
	char *d = (char *)dest;
	while (n--) { *d++ = *s++; }

	return dest;
}

char *strcpy(char *dest, const char *src) {
	const unsigned char *s = (const unsigned char *)src;
	unsigned char *d = (unsigned char *)dest;
	while ((*d++ = *s++));
	return dest;
}
/*
 * reverse string in place
 * http://mbed.org/forum/bugs-suggestions/topic/2319/?page=1#comment-19606
 */
void reverse(char *s) {
	char *j;
	int c;

	j = s + strlen(s) - 1;
	while (s < j) {
		c = *s;
		*s++ = *j;
		*j-- = c;
	}
}

/* itoa:  convert n to characters in s
 * http://mbed.org/forum/bugs-suggestions/topic/2319/?page=1#comment-19606
 *
 * */
void itoa(int n, char s[]) {
	int i, sign;

	if ((sign = n) < 0) /* record sign */
		n = -n; /* make n positive */
	i = 0;
	do { /* generate digits in reverse order */
		s[i++] = n % 10 + '0'; /* get next digit */
	} while ((n /= 10) > 0); /* delete it */
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

void sprintf_hex(char *dest, char *src, int len) {
	char *d = dest;
	char *s = src;
	while (len--) {
		if (((*s >> 4) & 0xF) < 10) {
			*d = '0' + ((*s >> 4) & 0xF);
		} else {
			*d = 'A' + ((*s >> 4) & 0xF) - 10;
		}
		d++;
		if ((*s & 0xF) < 10) {
			*d = '0' + (*s & 0xF);
		} else {
			*d = 'A' + (*s & 0xF) - 10;
		}
		d++;
		*d = ' ';

		d++;
		s++;
	}
}

void sprintf_i(char *dest, int i) {
	sprintf_is(dest, i, 0);
}

void sprintf_is(char *dest, int i, char *s) {
	sprintf_sis(dest, 0, i, s);
}

void sprintf_si(char *dest, char *s, int i) {
	sprintf_sis(dest, s, i, 0);
}

void sprintf_sis(char *dest, char *s1, int i, char *s2) {
	int slen1 = 0;
	int slen2 = 0;

	if (s1) {
		slen1 = strlen(s1);
		strcpy(dest, s1);
		dest[slen1] = '\0';
	}

	/* convert integer to string */
	itoa(i, &dest[slen1]);

	if (s2) {
		slen1 = strlen(dest);
		slen2 = strlen(s2);
		strcpy(&dest[slen1], s2);
		dest[slen1 + slen2] = '\0';
	}
}

void sprintf_isis(char *dest, int i1, char *s1, int i2, char *s2) {
	int slen1 = 0;
	int slen2 = 0;

	/* convert integer to string */
	itoa(i1, &dest[slen1]);

	slen1 = strlen(dest);
	if (s1) {
		strcpy(&dest[slen1], s1);
		slen1 += strlen(s1);
		dest[slen1] = '\0';
	}

	/* convert integer to string */
	itoa(i2, &dest[slen1]);

	if (s2) {
		slen1 = strlen(dest);
		slen2 = strlen(s2);
		strcpy(&dest[slen1], s2);
		dest[slen1 + slen2] = '\0';
	}
}
