/*
 * lcd.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "lcd.h"
#include "utils.h"
#include "delay.h"

void lcd_init(void) {

	/* set LCD pins to 1 (output), for now */
	GPIO_IODIR |= (LCD_DATA | LCD_EN | LCD_RW | LCD_RS);

	/* IO init complete, init LCD */

	/* init 4-bit ops*/
	lcd_rs_clr();
	lcd_rw_clr();
	lcd_en_clr();

	/* wait VDD raise > 4.5V, minimum 10ms */
	delay_ms(50);

	/* dummy inst */
	lcd_write_nibbles(0x30);
	lcd_write_nibbles(0x30);
	lcd_write_nibbles(0x30);

	/*
	 * FUNCTION SET
	 * 001DL  N F XX
	 * DL=1: 8bit
	 * DL=0: 4bit
	 * N=0: 1 line display
	 * N=1: 2 line display
	 * F=0: 5x7 dots
	 * F=1: 5x10 dots
	 *
	 * our case:
	 * 0010 1000
	 */
	lcd_en_set();
	lcd_out_data4(0x2);
	lcd_en_clr();
	lcd_wait();

	lcd_write_nibbles(0x28);

	/* LCD ON */
	lcd_write_nibbles(0x0E);

	/* clear display */
	lcd_write_nibbles(0x01);

	/* entry mode */
	lcd_write_nibbles(0x06);
}

void lcd_wait(void) {
	delay_us(500);
}

void lcd_out_data4(unsigned char val) {
	GPIO_IOCLR |= (LCD_DATA);
	GPIO_IOSET |= (val << 4);
}

void lcd_write_nibbles(unsigned char val) {

	/* high bits */
	lcd_en_set();
	lcd_out_data4((val >> 4) & 0x0F);
	lcd_en_clr();

	lcd_wait();

	/* lower bits */
	lcd_en_set();
	lcd_out_data4((val) & 0x0F);
	lcd_en_clr();

	lcd_wait();
}

void lcd_write_control(unsigned char val) {
	lcd_rs_clr();
	lcd_write_nibbles(val);
}

void lcd_putchar(unsigned char val) {

	lcd_rs_set();
	lcd_write_nibbles(val);
}

void lcd_print(const char *str) {
	int i;

	/* limit 1 line display for prints */
	for (i = 0; i < 16 && str[i] != 0; i++) {
		lcd_putchar(str[i]);
	}
}

void lcd_print_center(const char *str) {
	int i;
	int l = 0;
	int s = 0;
	while (str[l] != 0) {
		l++;
	}

	if (l < 16) {
		s = 8 - (l >> 1) - 1;
	}

	/* limit 1 line display for prints */
	for (i = 0; i < 16; i++) {
		if ((i < s) || (i >= (s + l))) {
			lcd_putchar(' ');
		} else {
			lcd_putchar(str[i - s]);
		}
	}
}

void lcd_print_line1(const char *s) {
	lcd_cursor_home();
	lcd_print_center(s);
}

void lcd_print_line2(const char *s) {
	int i;
	lcd_cursor_home();
	for (i = 0; i < 40; i++)
		lcd_cursor_right();
	lcd_print_center(s);
}

void lcd_print_both(const char *line1, const char *line2) {
	lcd_print_line1(line1);
	lcd_print_line2(line2);
}
