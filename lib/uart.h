/*
 * uart.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include "lpc210x_gnuarm.h"
#include "config.h"

/*
 * UART registers
 */

/* Modem control register */
#define UMCR_DTR				(1 << 0)	/* data transmit ready */
#define UMCR_RTS				(1 << 1)	/* request to send */
#define UMCR_LB					(1 << 4)	/* loopback */

/* Mode status register */
#define UMSR_DCTS				(1 << 0)	/* delta clear to send */
#define UMSR_DDSR				(1 << 1)	/* delta data set ready */
#define UMSR_TERI				(1 << 2)	/* trailing edge ring indicator */
#define UMSR_DDCD				(1 << 3)	/* delta data carrier detect */
#define UMSR_CTS				(1 << 4)	/* clear to send */
#define UMSR_DSR				(1 << 5)	/* data set ready */
#define UMSR_RI					(1 << 6)	/* ring indicator */
#define UMSR_DCD				(1 << 7)	/* data carrier detect */

/* FIFO control register */
#define UFCR_EN					(1 << 0)	/* FIFO enable */
#define UFCR_RX_RST				(1 << 1)	/* reset receive FIFO */
#define UFCR_TX_RST				(1 << 2)	/* reset transmit FIFO */
#define UFCR_TRIG_1				(0 << 6)	/* trigger when 1 character in FIFO */
#define UFCR_TRIG_4				(1 << 6)	/* trigger when 4 characters in FIFO */
#define UFCR_TRIG_8				(2 << 6)	/* trigger when 8 characters in FIFO */
#define UFCR_TRIG_14			(3 << 6)	/* trigger when 14 characters in FIFO */

/* Line control register */
#define ULCR_CHAR_5				(0 << 0)	/* 5-bit character length */
#define ULCR_CHAR_6				(1 << 0)	/* 6-bit character length */
#define ULCR_CHAR_7				(2 << 0)	/* 7-bit character length */
#define ULCR_CHAR_8				(3 << 0)	/* 8-bit character length */
#define ULCR_STOP_1				(0 << 2)	/* 1 stop bit */
#define ULCR_STOP_2				(1 << 2)	/* 2 stop bits */
#define ULCR_PAR_NO				(0 << 3)	/* no parity */
#define ULCR_PAR_ODD			(1 << 3)	/* odd parity */
#define ULCR_PAR_EVEN			(3 << 3)	/* even parity */
#define ULCR_PAR_MARK			(5 << 3)	/* mark "1" parity */
#define ULCR_PAR_SPACE			(7 << 3)	/* space "0" parity */
#define ULCR_BREAK_EN			(1 << 6)	/* output BREAK line */
#define ULCR_DLAB_EN			(1 << 7)	/* enable divisor latch access */

/* Line status register */
#define ULSR_RDR				(1 << 0)	/* receive data ready */
#define ULSR_OE					(1 << 1)	/* overrun error */
#define ULSR_PE					(1 << 2)	/* parity error */
#define ULSR_FE					(1 << 3)	/* framing error */
#define ULSR_BI					(1 << 4)	/* break interrupt */
#define ULSR_THRE				(1 << 5)	/* transmit holding register empty */
#define ULSR_TEMT				(1 << 6)	/* transmit empty */
#define ULSR_RXFE				(1 << 7)	/* error in receive FIFO */
#define ULSR_ERR_MASK			0x1E		/* error mask bits */

/* Interrupt enable register */
#define UIER_ERBFI				(1 << 0)	/* enable RDA interrupt - receive data available */
#define UIER_ETBEI				(1 << 1)	/* enable THRE interrupt - transmit holding register empty */
#define UIER_ELSI				(1 << 2)	/* enable RLS interrupt - receive line status */
#define UIER_EDSSI				(1 << 3)	/* enable MS interrupt - modem status */

/* Interrupt ID register */
#define UIIR_NO_INT				(1 << 0)	/* no interrupts pending */
#define UIIR_MS_INT				(0 << 1)	/* MS interrupt - modem status */
#define UIIR_THRE_INT			(1 << 1)	/* THRE interrupt - transmit holding register empty */
#define UIIR_RDA_INT			(2 << 1)	/* RDA interrupt - receive data available */
#define UIIR_RLS_INT			(3 << 1)	/* RLS interrupt - receive line status */
#define UIIR_CTI_INT			(6 << 1)	/* character timeout indicator */
#define UIIR_ID_MASK			0x0E		/* interrupt ID mask */


/* Baud-Rate is calculated based on peripheral clock (pclk)
 * the divisor must be 16 times the desired baud rate */

#define UART_BAUD(baud) (uint16_t) \
	(((F_OSC * PLL_M / VPBDIV_VAL) / ((baud) * 16.0)) + 0.5)

/* Constants for uart0_init() and uart1_init() functions:
 * Example: uart0_init(baud, mode, fmode)
 */
/* 'baud' values */
#define B1200			UART_BAUD(1200)
#define B9600			UART_BAUD(9600)
#define B19200			UART_BAUD(19200)
#define B38400			UART_BAUD(38400)
#define B57600			UART_BAUD(57600)
#define B115200			UART_BAUD(115200)

/* 'mode' settings */
#define UART_8N1		(uint8_t)(ULCR_CHAR_8 | ULCR_PAR_NO   | ULCR_STOP_1)
#define UART_7N1		(uint8_t)(ULCR_CHAR_7 | ULCR_PAR_NO   | ULCR_STOP_1)
#define UART_8N2		(uint8_t)(ULCR_CHAR_8 | ULCR_PAR_NO   | ULCR_STOP_2)
#define UART_7N2		(uint8_t)(ULCR_CHAR_7 | ULCR_PAR_NO   | ULCR_STOP_2)
#define UART_8E1		(uint8_t)(ULCR_CHAR_8 | ULCR_PAR_EVEN | ULCR_STOP_1)
#define UART_7E1		(uint8_t)(ULCR_CHAR_7 | ULCR_PAR_EVEN | ULCR_STOP_1)
#define UART_8E2		(uint8_t)(ULCR_CHAR_8 | ULCR_PAR_EVEN | ULCR_STOP_2)
#define UART_7E2		(uint8_t)(ULCR_CHAR_7 | ULCR_PAR_EVEN | ULCR_STOP_2)
#define UART_8O1		(uint8_t)(ULCR_CHAR_8 | ULCR_PAR_ODD  | ULCR_STOP_1)
#define UART_7O1		(uint8_t)(ULCR_CHAR_7 | ULCR_PAR_ODD  | ULCR_STOP_1)
#define UART_8O2		(uint8_t)(ULCR_CHAR_8 | ULCR_PAR_ODD  | ULCR_STOP_2)
#define UART_7O2		(uint8_t)(ULCR_CHAR_7 | ULCR_PAR_ODD  | ULCR_STOP_2)

/* 'fmode' settings */
#define UART_FIFO_OFF	(0x00)
#define UART_FIFO_1		(uint8_t)(UFCR_EN | UFCR_TRIG_1)
#define UART_FIFO_4		(uint8_t)(UFCR_EN | UFCR_TRIG_4)
#define UART_FIFO_8		(uint8_t)(UFCR_EN | UFCR_TRIG_8)
#define UART_FIFO_14	(uint8_t)(UFCR_EN | UFCR_TRIG_14)

#endif /* UART_H_ */
