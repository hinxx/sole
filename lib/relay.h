/*
 * relay.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef RELAY_H_
#define RELAY_H_

#define RELAY_PIN		13
#define RELAY_MASK		(1 << RELAY_PIN)

void relay_init(void);
void relay_open(void);
void relay_close(void);

#endif /* RELAY_H_ */
