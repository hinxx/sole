/*
 * i2c_eeprom.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef I2C_EEPROM_H_
#define I2C_EEPROM_H_

void i2c_eeprom_init(void);
int i2c_eeprom_write(unsigned char offset, unsigned char data);
int i2c_eeprom_read(unsigned char offset, unsigned char *data);

#endif /* I2C_EEPROM_H_ */
