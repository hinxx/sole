/*
 * button.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "button.h"

static buttons btns = {
		.up = 0,		/* BUTTON1 */
		.left = 0,		/* BUTTON2 */
		.center = 0,	/* BUTTON3 */
		.right = 0,		/* BUTTON4 */
		.down = 0,		/* BUTTON5 */
		.count = 0,
};

void button_init(void) {
	/* set button pins to 0 (input) */
	GPIO_IODIR &= ~(BUTTON1 | BUTTON2 | BUTTON3 | BUTTON4 | BUTTON5);
}

buttons *button_states() {

	/* buttons are active low */
	btns.up = 		(GPIO_IOPIN & BUTTON1) ? 0 : 1;
	btns.left = 	(GPIO_IOPIN & BUTTON2) ? 0 : 1;
	btns.center =	(GPIO_IOPIN & BUTTON3) ? 0 : 1;
	btns.right =	(GPIO_IOPIN & BUTTON4) ? 0 : 1;
	btns.down =		(GPIO_IOPIN & BUTTON5) ? 0 : 1;
	btns.count =	(btns.up + btns.left
					+ btns.center + btns.right
					+ btns.down);
	return &btns;
}
