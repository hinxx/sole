/*
 * led.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "led.h"

void led_init(void) {
	/* set up LED pin direction to output */
	GPIO_IODIR |= LED_MASK;
}

void led_on(void) {
	/* LED on */
	GPIO_IOCLR |= LED_MASK;
}

void led_off(void) {
	/* LED off */
	GPIO_IOSET |= LED_MASK;
}

void led_toggle(void){
	/* rely on the current LED pin state */
	if (GPIO_IOPIN & LED_MASK) {
		/* LED off */
		GPIO_IOCLR |= LED_MASK;
	} else {
		/* LED on */
		GPIO_IOSET |= LED_MASK;
	}
}
