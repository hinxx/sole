/*
 * sle4442.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "sle4442.h"
#include <utils.h>
#include <uart0.h>
#include <delay.h>
#include <debug.h>

#define io_set()		(GPIO_IOSET |= SLE4442_IO)
#define io_clr()		(GPIO_IOCLR |= SLE4442_IO)
#define io_in()			(GPIO_IODIR &= ~SLE4442_IO)
#define io_out()		(GPIO_IODIR |= SLE4442_IO)
#define io_val()		(GPIO_IOPIN & SLE4442_IO)

#define clk_out()		(GPIO_IODIR |= SLE4442_CLK)
#define clk_set()		(GPIO_IOSET |= SLE4442_CLK)
#define clk_clr()		(GPIO_IOCLR |= SLE4442_CLK)

#define rst_out()		(GPIO_IODIR |= SLE4442_RST)
#define rst_set()		(GPIO_IOSET |= SLE4442_RST)
#define rst_clr()		(GPIO_IOCLR |= SLE4442_RST)

#define det_in()		(GPIO_IODIR &= ~SLE4442_DET)
#define det_val()		(GPIO_IOPIN & SLE4442_DET)

static void start_cond(void);
static void stop_cond(void);
static int32_t send_data(const uint8_t *data, uint32_t len);
static int32_t recv_data(uint8_t *data, uint32_t len);
static void busy_wait(void);
static int32_t read_mmem(uint8_t *data);
static int32_t update_mmem(uint32_t address, uint8_t data);
static int32_t read_smem(uint8_t *data);
static int32_t update_smem(uint8_t address, uint8_t data);
static int32_t compare_smem(uint8_t address, uint8_t data);


/* SLE4442 data scratch buffer */
static uint8_t mem_buf[300] = {0};
static uint8_t hex[1024] = {0};


void sle4442_init(void) {

	/* set all pins low */
	rst_clr();
	clk_clr();
	io_clr();

	/* set all pins to output */
	io_out();
	rst_out();
	clk_out();
	det_in();

	D_SLE4442NL("initialized!");
}

int32_t sle4442_detect(void) {
	/* DET pin is low if card is inserted */
	if (det_val() == 0) {
		return 1;
	}
	return 0;
}

int32_t sle4442_reset(void) {
	/* set IO pin to input */
	io_in();
	io_set();

	/* generate card reset pulse with one clock pulse between */
	clk_clr();
	rst_set();
	delay_us(200);
	clk_set();
	delay_us(10);
	clk_clr();
	delay_us(10);
	rst_clr();
	delay_us(10);

	/* read 4 byte response - ATR */
	recv_data(mem_buf, SLE4442_ATR_LEN);

#ifdef DEBUG_SLE4442
	D_SLE4442("ATR[4]: ");
	char hex[16] = {0};
	sprintf_hex(hex, mem_buf, 4);
	uart0_puts(hex);
	uart0_puts("\r\n");
#endif

	/* for SLE4442 memory cards ATR is: 0xA2 0x13 0x10 0x91 */
	if (mem_buf[0] == 0xA2 && mem_buf[1] == 0x13 &&
			mem_buf[2] == 0x10 && mem_buf[3] == 0x91) {
		return 0;
	}

	return -1;
}

int32_t sle4442_read(uint8_t *mmem, uint8_t *smem) {

	read_mmem(mmem);
#ifdef DEBUG_SLE4442
	D_SLE4442("MMEM[265]: ");
	sprintf_hex(hex, mmem, SLE4442_MMEM_SZ);
	uart0_puts(hex);
	uart0_puts("\r\n");
#endif

	read_smem(smem);
#ifdef DEBUG_SLE4442
	D_SLE4442("SMEM[4]: ");
	memset(hex, 0, 1024);
	sprintf_hex(hex, smem, SLE4442_SMEM_SZ);
	uart0_puts(hex);
	uart0_puts("\r\n");
#endif

	/* for SLE4442 memory cards ATR is: 0xA2 0x13 0x10 0x91 */
	if (mmem[0] == 0xA2 && mmem[1] == 0x13 &&
			mmem[2] == 0x10 && mmem[3] == 0x91) {
		return 0;
	}

	return -1;
}

int32_t sle4442_update_block(uint32_t address, uint8_t *mmem, uint32_t len) {

	if (address < 4) {
		return -1;
	}

	if ((address + len) > SLE4442_MMEM_SZ) {
		return -1;
	}

	while (len--) {
		update_mmem(address++, *mmem++);
	}

	return 0;
}

int32_t sle4442_verify_pin(uint8_t *pin) {
	uint8_t currEC, cardEC, newEC;
	uint8_t smem_buf[4] = {0};

#ifdef DEBUG_SLE4442
	D_SLE4442("PIN[3]: ");
	memset(hex, 0, 1024);
	sprintf_hex(hex, pin, 3);
	uart0_puts(hex);
	uart0_puts("\r\n");
#endif

	currEC = SLE4442_PSC_ERR;
	newEC = SLE4442_PSC_ERR;

	/* read 4 bytes of security memory
	 * NOTE: at this time card PIN might not be yet verified, and only first
	 *       byte of reply is valid (EC value), PIN bytes will be 0 */
	read_smem(smem_buf);
	cardEC = *(&smem_buf[0]);
#ifdef DEBUG_SLE4442
	D_SLE4442("SMEM[4]: ");
	memset(hex, 0, 1024);
	sprintf_hex(hex, smem_buf, SLE4442_SMEM_SZ);
	uart0_puts(hex);
	uart0_puts("\r\n");
#endif

	switch (cardEC & SLE4442_PSC_MASK) {
		case SLE4442_PSC_EC1:
			/* XXX: only one try available - if next PIN verification fails
			 * the card is lost, forever! */
			currEC = SLE4442_PSC_EC1;
			newEC = SLE4442_PSC_EC0;
			break;
		case SLE4442_PSC_EC3:
			/* WARNING: two tries available */
			currEC = SLE4442_PSC_EC3;
			newEC = SLE4442_PSC_EC1;
			break;
		case SLE4442_PSC_EC7:
			/* good, all three tries available */
			currEC = SLE4442_PSC_EC7;
			newEC = SLE4442_PSC_EC3;
			break;
	}

#ifdef DEBUG_SLE4442
	D_SLE4442("CURR EC ");
	memset(hex, 0, 1024);
	sprintf_hex(hex, &currEC, 1);
	uart0_puts(hex);
	uart0_puts("\r\n");
	D_SLE4442("NEW  EC ");
	memset(hex, 0, 1024);
	sprintf_hex(hex, &newEC, 1);
	uart0_puts(hex);
	uart0_puts("\r\n");
#endif

	if (newEC == SLE4442_PSC_EC0) {
		/* we have not set new EC - possibly due to last try left */
		D_SLE4442NL("Refusing to waste last attempt!");
		return -1;
	}

	/* starting verification cycle */
	/* 1. update EC -- zero-out one bit */
	update_smem(0, newEC);

	/* 2. compare user PIN bytes with card PIN bytes */
	compare_smem(1, pin[0]);
	compare_smem(2, pin[1]);
	compare_smem(3, pin[2]);

	/* 3. update EC -- reset to 0x07 */
	update_smem(0, 0xFF);

	/* finished verification cycle */

	/* read PIN again - this time we should see PIN bytes in response */
	read_smem(smem_buf);
	cardEC = *(&smem_buf[0]);
#ifdef DEBUG_SLE4442
	D_SLE4442("SMEM[4]: ");
	memset(hex, 0, 1024);
	sprintf_hex(hex, smem_buf, SLE4442_SMEM_SZ);
	uart0_puts(hex);
	uart0_puts("\r\n");
#endif

#ifdef DEBUG_SLE4442
	D_SLE4442("CARD EC ");
	memset(hex, 0, 1024);
	sprintf_hex(hex, &cardEC, 1);
	uart0_puts(hex);
	uart0_puts("\r\n");
#endif

	/* see if verification succeeded - EC should be 7 */
	if (cardEC == 0xFF) {
		/* XXX: BAD! verification was unsuccessful */
		D_SLE4442NL("verification was unsuccessful - EC == 0xFF!");
		return -1;
	}

	/* see if verification succeeded - EC should be 7 */
	if ((cardEC & SLE4442_PSC_MASK) != SLE4442_PSC_EC7) {
		/* XXX: BAD! verification was unsuccessful */
		D_SLE4442NL("verification was unsuccessful - EC != 0x07!");
		return -1;
	}

	D_SLE4442NL("verification was SUCCESSFUL!");
	return 0;
}



static void start_cond(void) {
	/* set IO pin to output */
	io_out();
	/* prepare for start condition */
	io_set();
	clk_clr();
	delay_us(10);

	/* generate IFD start condition */
	clk_set();
	delay_us(100);
	io_clr();
	delay_us(100);

	/* reset CLK and IO */
	io_clr();
	delay_us(100);
	clk_clr();
	delay_us(100);
}

static void stop_cond(void) {
	/* set IO pin to output */
	io_out();
	/* prepare for stop condition */
	io_clr();
	clk_clr();
	delay_us(10);

	/* generate IFD stop condition */
	clk_set();
	delay_us(100);
	io_set();
	delay_us(100);

	/* reset CLK and IO */
	io_clr();
	delay_us(100);
	clk_clr();
	delay_us(100);
}

static int32_t send_data(const uint8_t *data, uint32_t len) {

	uint32_t byte, bit;
	const uint8_t *p = data;

	for (byte = 0; byte < len; byte++) {
		for (bit = 0; bit < 8; bit++) {
			/* set CLK low */
			clk_clr();

			delay_us(10);
			/* set or clean IO pin */
			if (*p & (1 << bit)) {
				io_set();
			} else {
				io_clr();
			}
			delay_us(10);

			/* set CLK high */
			clk_set();
			delay_us(20);
		}

		/* next byte to handle*/
		p++;
	}

	/* set CLK low - mandatory! */
	clk_clr();

	return len;
}

static int32_t recv_data(uint8_t *data, uint32_t len) {

	uint32_t byte, bit;
	uint8_t *p = data;

	/* set IO to input */
	io_in();
	/* set IO high */
	io_set();
	delay_us(20);

	for (byte = 0; byte < len; byte++) {
		/* clear value */
		*p = 0;

		for (bit = 0; bit < 8; bit++) {
			/* set CLK high */
			clk_set();

			delay_us(10);
			/* read IO pin level */
			if (io_val()) {
				*p |= (1 << bit);
			} else {
				*p &= ~(1 << bit);
			}
			delay_us(10);

			/* set CLK low */
			clk_clr();
			delay_us(20);
		}

		/* next byte to handle*/
		p++;
	}

	/* extra clock for card to set IO pin high */
	/* set CLK high */
	clk_set();
	delay_us(20);
	/* set CLK low */
	clk_clr();
	delay_us(20);

	return len;
}

static void busy_wait(void) {
	D_SLE4442("enter\r\n");

	/* set IO to input */
	io_in();
	/* set IO high */
	io_set();
	/* set CLK high */
	clk_set();
	delay_us(20);

	int clocks = 0;
	do {
		/* set CLK high */
		clk_set();
		delay_us(20);
		/* set CLK low */
		clk_clr();
		delay_us(20);
		clocks++;
	} while (! io_val());

#ifdef DEBUG_SLE4442
	D_SLE4442("waiting took ..");
	char buf[4] = {0};
	sprintf_i(buf, clocks);
	uart0_puts(buf);
	uart0_puts("\r\n");
#endif
}

static int32_t read_mmem(uint8_t *data) {
	D_SLE4442("enter\r\n");

	mem_buf[0] = SLE4442_RD_MMEM;
	mem_buf[1] = 0;
	mem_buf[2] = 0;

	start_cond();
	send_data(mem_buf, SLE4442_CMD_LEN);
	stop_cond();

	recv_data(data, SLE4442_MMEM_SZ);

	return SLE4442_MMEM_SZ;
}

static int32_t update_mmem(uint32_t address, uint8_t data) {
	D_SLE4442("enter\r\n");

	mem_buf[0] = SLE4442_UP_MMEM;
	mem_buf[1] = (uint8_t) address;
	mem_buf[2] = data;

	start_cond();
	send_data(mem_buf, SLE4442_CMD_LEN);
	stop_cond();

	/* let the card finish */
	busy_wait();

	return 0;
}

static int32_t read_smem(uint8_t *data) {
	D_SLE4442("enter\r\n");

	mem_buf[0] = SLE4442_RD_SMEM;
	mem_buf[1] = 0;
	mem_buf[2] = 0;

	start_cond();
	send_data(mem_buf, SLE4442_CMD_LEN);
	stop_cond();

	recv_data(data, SLE4442_SMEM_SZ);

	return SLE4442_SMEM_SZ;
}

static int32_t update_smem(uint8_t address, uint8_t data) {
#ifdef DEBUG_SLE4442
	D_SLE4442("enter data : 0x");
	char buf[4] = {0};
	sprintf_hex(buf, &data, 1);
	uart0_puts(buf);
	uart0_puts("\r\n");
#endif

	mem_buf[0] = SLE4442_UP_SMEM;
	mem_buf[1] = address;
	mem_buf[2] = data;

	start_cond();
	send_data(mem_buf, SLE4442_CMD_LEN);
	stop_cond();

	/* let the card finish */
	busy_wait();

	return 0;
}

static int32_t compare_smem(uint8_t address, uint8_t data) {
#ifdef DEBUG_SLE4442
	D_SLE4442("enter data : 0x");
	char buf[4] = {0};
	sprintf_hex(buf, &data, 1);
	uart0_puts(buf);
	uart0_puts("\r\n");
#endif

	mem_buf[0] = SLE4442_CMP_DATA;
	mem_buf[1] = address;
	mem_buf[2] = data;

	start_cond();
	send_data(mem_buf, SLE4442_CMD_LEN);
	stop_cond();

	/* let the card finish */
	busy_wait();

	return 0;
}
