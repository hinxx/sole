/*
 * buzzer.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "buzzer.h"

void buzzer_init(void) {
	/* set buzzer port pin to 1 */
	GPIO_IODIR |= BUZZER;
}

void buzzer_buzz(int repeat, int delay_on, int delay_off) {
	int i, j;

	i = repeat;
	while (i--) {
		/* activate buzzer */
		GPIO_IOCLR |= BUZZER;
		for (j = 0; j < delay_on; j++)
			;
		/* deactivate buzzer */
		GPIO_IOSET |= BUZZER;
		for (j = 0; j < delay_off; j++)
			;
	}
}

void buzzer_beep(void) {
	/* do a beep */
	buzzer_buzz(300, 500, 1000);
}

void buzzer_long_beep(void) {
	/* do a beep */
	buzzer_buzz(500, 1000, 1000);
}

void buzzer_short_beep(void) {
	/* do a beep */
	buzzer_buzz(200, 200, 500);
}
