/*
 * i2c.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef I2C_H_
#define I2C_H_

#include "lpc210x_gnuarm.h"

typedef struct {
	unsigned char data[16];
	unsigned short len;
	unsigned short cnt;
	unsigned char stop;
	unsigned char nack;
} i2cdev_t;

/* I2C SDA-Pin=P0.3, SCL-Pin=P0.2
 * PINSEL0 has to be set to "I2C-Function" = Function "01" for Pin 0.2 and 0.3
 */

#define PINSEL_I2C_SCL		4
#define PINSEL_I2C_SDA		6

/* PINSEL0 bits to activate I2C */
#define I2C_PINSEL			((0x01 << PINSEL_I2C_SCL) | (0x01 << PINSEL_I2C_SDA))
/* I2C PINSEL0 mask */
#define I2C_PINMASK			(0x000000F0)

/* I2C interrupt enable bit in VIC */
#define I2C_IntEnBit		9

#define I2C_AA				(1 << 2)
#define I2C_SI				(1 << 3)
#define I2C_STO				(1 << 4)
#define I2C_STA				(1 << 5)
#define I2C_I2EN			(1 << 6)

/* I2C status bits */

/* A START condition has been transmitted */
#define I2C_START			0x08
/* A repeated START condition has been transmitted */
#define I2C_RESTART			0x10
/* SLA+W has been transmitted; ACK has been received */
#define I2C_ACK				0x18
/* SLA+W has been  transmitted; NOT ACK has been received */
#define I2C_NACK			0x20
/* Data byte in S1DAT has been transmitted; ACK has been received */
#define I2C_DATA_ACK		0x28
/* Data byte in S1DAT has been transmitted; NOT ACK has been received */
#define I2C_DATA_NACK		0x30
/* Arbitration lost in SLA+R/W or Data bytes */
#define I2C_LOST			0x30

void i2c_init(unsigned short cll_val, unsigned short clh_val);
void i2c_handler(unsigned long *ptr);
int i2c_master_send(i2cdev_t *i2cdev);
int i2c_master_recv(i2cdev_t *i2cdev);

#endif /* I2C_H_ */
