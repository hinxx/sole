/*
 * uart0.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef UART0_H_
#define UART0_H_

#include "lpc210x_gnuarm.h"
#include "config.h"
#include "uart.h"


/* UART0 TX-Pin=P0.2, RX-Pin=P0.1
 * PINSEL0 has to be set to "UART-Function" = Function "01" for Pin 0.0 and 0.1
 */

#define PINSEL_TXD0			0
#define PINSEL_RXD0			2

/* PINSEL0 bits to activate UART0 */
#define UART0_PINSEL		((0x01 << PINSEL_TXD0) | (0x01 << PINSEL_RXD0))
/* UART0 PINSEL0 mask */
#define UART0_PINMASK		(0x0000000F)

/* UART0 interrupt enable bit in VIC */
#define UART0_IntEnBit		6

void uart0_init(uint16_t baud, uint8_t mode, uint8_t fmode);
int uart0_putc(int ch);
uint16_t uart0_space(void);
const char *uart0_puts(const char *str);
int uart0_tx_empty(void);
void uart0_tx_flush(void);
int uart0_getc(void);
void uart0_set_handler(unsigned long *ptr);

#if 0
unsigned char uart0_tx_char(unsigned char inchar);
void uart0_tx_send();
#endif

#endif /* UART0_H_ */
