/*
 * i2c.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "i2c.h"
#include "uart0.h"
#include "delay.h"
#include "debug.h"

#ifdef DEBUG_I2C
static uint8_t hex_buf[1024] = {0};
#endif

void i2c_init(unsigned short cll_val, unsigned short clh_val) {
	/* setup Pin Function Select Register (Pin Connect Block) */
	PCB_PINSEL0 = (PCB_PINSEL0 & ~I2C_PINMASK) | I2C_PINSEL;

	/* clear all I2C settings */
	I2C_I2CONCLR = 0x0000006C;

	/* Set bit rate to desired value - see LPC2106 data sheet */
	I2C_I2SCLH = clh_val;
	I2C_I2SCLL = cll_val;
	D_I2CNL("initialized!");
}

void i2c_set_handler(unsigned long *ptr) {

	/* enable I2C interface */
	I2C_I2CONSET = 0x00000040;

	/* set up VIC */

	/* set priority 6 for I2C */
	VICVectCntl6 = 0x00000020 | I2C_IntEnBit;
	/* assign ISR routine */
	VICVectAddr6 = (unsigned long) ptr;
	/* enable I2C VIC */
	VICIntEnable = (1 << I2C_IntEnBit);
}

void i2c_master_send_cont(i2cdev_t *i2cdev) {
	if (i2cdev->cnt == 0) {
#ifdef DEBUG_I2C
		D_I2C("1 START, data: 0x");
		sprintf_hex(hex_buf, &i2cdev->data[i2cdev->cnt], 1);
		uart0_puts(hex_buf);
		uart0_putc(' ');
		uart0_putc(i2cdev->cnt + '0');
		uart0_puts("\r\n");
#endif
		/* send device address byte */
		I2C_I2DAT = i2cdev->data[i2cdev->cnt++];
		/* clear STA and SI flag */
		I2C_I2CONCLR = I2C_STA | I2C_SI;
	} else if (i2cdev->cnt == i2cdev->len) {
		D_I2CNL("2 STOP, ending.. ");
		i2cdev->stop = 1;
		/* set STO flag */
		I2C_I2CONSET = I2C_STO;
		/* clear SI flag */
		I2C_I2CONCLR = I2C_SI;
	} else {
		D_I2C("3 CONT, data: 0x");
#ifdef DEBUG_I2C
		sprintf_hex(hex_buf, &i2cdev->data[i2cdev->cnt], 1);
		uart0_puts(hex_buf);
		uart0_putc(' ');
		uart0_putc(i2cdev->cnt + '0');
		uart0_puts("\r\n");
#endif
		/* send address / data byte */
		I2C_I2DAT = i2cdev->data[i2cdev->cnt++];
		/* clear SI flag */
		I2C_I2CONCLR = I2C_SI;
	}
}

void i2c_master_send_stop(i2cdev_t *i2cdev) {
	D_I2CNL("enter");

	i2cdev->nack = 1;
	i2cdev->stop = 1;

	/* set STO flag */
	I2C_I2CONSET = I2C_STO;
	/* clear SI flag */
	I2C_I2CONCLR = I2C_SI;
}

int i2c_master_send(i2cdev_t *i2cdev) {
	D_I2CNL("enter");
	unsigned char stat;
	int retry = 1000;

	i2cdev->stop = 0;
	i2cdev->cnt = 0;
	i2cdev->nack = 0;

	/* clear all I2C settings */
	I2C_I2CONCLR = 0x0000006C;
	/* enable the I2C interface */
	I2C_I2CONSET = I2C_I2EN;

	/* set STA flag */
	I2C_I2CONSET = I2C_I2EN | I2C_STA;

	while (! i2cdev->stop) {
		/* wait for SI set */
		do {
			if (retry-- <= 0) {
				return 1;
			}
			delay_us(1);
		} while (! (I2C_I2CONSET & I2C_SI));

		/* read STAT */
		stat = I2C_I2STAT;
#ifdef DEBUG_I2C
		D_I2C("stat: 0x");
		sprintf_hex(hex_buf, &stat, 1);
		uart0_puts(hex_buf);
		uart0_puts("\r\n");
#endif
		/* see 80C51 Family Derivatives 8XC552/562 overview, Table 3.
		 * (Master Transmitter Mode) for I2C states definitions */
		switch (stat) {
			case 0x08 :
				i2c_master_send_cont(i2cdev);
				break;
			case 0x10 :
				i2c_master_send_cont(i2cdev);
				break;
			case 0x18 :
				i2c_master_send_cont(i2cdev);
				break;
			case 0x28 :
				i2c_master_send_cont(i2cdev);
				break;

			case 0x00 :
				i2c_master_send_stop(i2cdev);
				break;
			case 0x20 :
				i2c_master_send_stop(i2cdev);
				break;
			case 0x30 :
				i2c_master_send_stop(i2cdev);
				break;

			default :
				break;
		}
	}

	/* success if NACK was NOT received */
	return i2cdev->nack;
}

void i2c_master_recv_cont(i2cdev_t *i2cdev) {

	if (i2cdev->cnt == 0) {
		D_I2CNL("1 ACK");
		/* set ACK flag */
		I2C_I2CONSET = I2C_AA;
		/* clear SI flag */
		I2C_I2CONCLR = I2C_SI;

		/* not incrementing i2cdev->cnt now will place first data byte into
		 * data index 1 */
	} else if (i2cdev->cnt == i2cdev->len) {
		/* get data byte */
		i2cdev->data[i2cdev->cnt] = I2C_I2DAT;
#ifdef DEBUG_I2C
		D_I2C("2 ACK, ending, data: 0x");
		sprintf_hex(hex_buf, &i2cdev->data[i2cdev->cnt], 1);
		uart0_puts(hex_buf);
		uart0_putc(' ');
		uart0_putc(i2cdev->cnt + '0');
		uart0_puts("\r\n");
#endif
		/* clear SI and ACK flag */
		I2C_I2CONCLR = I2C_AA | I2C_SI;
	} else {
		i2cdev->data[i2cdev->cnt] = I2C_I2DAT;
#ifdef DEBUG_I2C
		D_I2C("3 CONT, data: 0x");
		sprintf_hex(hex_buf, &i2cdev->data[i2cdev->cnt], 1);
		uart0_puts(hex_buf);
		uart0_putc(' ');
		uart0_putc(i2cdev->cnt + '0');
		uart0_puts("\r\n");
#endif
		i2cdev->cnt++;
		/* set ACK flag */
		I2C_I2CONSET = I2C_AA;
		/* clear SI flag */
		I2C_I2CONCLR = I2C_SI;
	}
}

void i2c_master_recv_stop(i2cdev_t *i2cdev) {

	i2cdev->data[i2cdev->cnt] = I2C_I2DAT;

#ifdef DEBUG_I2C
	D_I2C("data: 0x");
	sprintf_hex(hex_buf, &i2cdev->data[i2cdev->cnt], 1);
	uart0_puts(hex_buf);
	uart0_putc(' ');
	uart0_putc(i2cdev->cnt + '0');
	uart0_puts("\r\n");
#endif

	i2cdev->stop = 1;

	/* set STO and ACK flag */
	I2C_I2CONSET = I2C_STO | I2C_AA;
	/* clear SI flag */
	I2C_I2CONCLR = I2C_SI;
}

int i2c_master_recv(i2cdev_t *i2cdev) {
	D_I2CNL("enter");
	unsigned char stat;
	int retry = 1000;

	i2cdev->stop = 0;
	i2cdev->cnt = 0;
	i2cdev->nack = 0;

	/* clear all I2C settings */
	I2C_I2CONCLR = 0x000000FF;
	/* enable the I2C interface */
	I2C_I2CONSET = I2C_I2EN;

	/* set STA flag */
	I2C_I2CONSET = I2C_I2EN | I2C_STA;

	while (! i2cdev->stop) {
		/* wait for SI set */
		do {
			if (retry-- <= 0) {
				return 1;
			}
			delay_us(1);
		} while (! (I2C_I2CONSET & I2C_SI));

		/* read STAT */
		stat = I2C_I2STAT;
#ifdef DEBUG_I2C
		D_I2C("stat: 0x");
		sprintf_hex(hex_buf, &stat, 1);
		uart0_puts(hex_buf);
		uart0_puts("\r\n");
#endif
		/* see 80C51 Family Derivatives 8XC552/562 overview, Table 4.
		 * (Master Receiver Mode) for I2C states definitions */
		switch (stat) {
			case 0x08 :
				i2c_master_send_cont(i2cdev);
				break;
			case 0x10 :
				i2c_master_send_cont(i2cdev);
				break;
			case 0x40 :
				i2c_master_recv_cont(i2cdev);
				break;
			case 0x50 :
				i2c_master_recv_cont(i2cdev);
				break;
			case 0x58 :
				i2c_master_recv_stop(i2cdev);
				break;

			default :
				break;
		}
	}

	return 0;
}
