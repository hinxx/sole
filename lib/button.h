/*
 * button.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef BUTTON_H_
#define BUTTON_H_

#define BUTTON1_PIN			27
#define BUTTON2_PIN			28
#define BUTTON3_PIN			29
#define BUTTON4_PIN			30
#define BUTTON5_PIN			31

#define BUTTON1				(1 << BUTTON1_PIN)
#define BUTTON2				(1 << BUTTON2_PIN)
#define BUTTON3				(1 << BUTTON3_PIN)
#define BUTTON4				(1 << BUTTON4_PIN)
#define BUTTON5				(1 << BUTTON5_PIN)

typedef struct {
	unsigned char up;		/* BUTTON1 */
	unsigned char left;		/* BUTTON2 */
	unsigned char center;	/* BUTTON3 */
	unsigned char right;	/* BUTTON4 */
	unsigned char down;		/* BUTTON5 */
	unsigned char count;	/* number of pressed buttons */
} buttons;

void button_init(void);
buttons *button_states();

#endif /* BUTTON_H_ */
