/*
 * debug.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef DEBUG_H_
#define DEBUG_H_

/* UART0 is used for debugging output, it can still be used for other purposes */
#include "uart0.h"

/* enable debugging output if any of the DEBUG_xxx directive is enabled */
#if defined(DEBUG_I2C) || \
	defined(DEBUG_EEPROM) || \
	defined(DEBUG_SLE4442) || \
	defined(DEBUG_USER) || \
	defined(DEBUG_SUPER)
 #define D(x)					{ uart0_puts(__FUNCTION__); \
								  uart0_puts(": "); \
								  uart0_puts(x); }
 #define DNL(x)					{ D(x); uart0_puts("\r\n"); }
#else
 #define D(x)					{}
 #define DNL(x)					{}
#endif

#ifdef DEBUG_I2C
 #define D_I2C(x)				D(x)
 #define D_I2CNL(x)				DNL(x)
#else
 #define D_I2C(x)				{}
 #define D_I2CNL(x)				{}
#endif

#ifdef DEBUG_EEPROM
 #define D_EEPROM(x)			D(x)
 #define D_EEPROMNL(x)			DNL(x)
#else
 #define D_EEPROM(x)			{}
 #define D_EEPROMNL(x)			{}
#endif

#ifdef DEBUG_SLE4442
 #define D_SLE4442(x)			D(x)
 #define D_SLE4442NL(x)			DNL(x)
#else
 #define D_SLE4442(x)			{}
 #define D_SLE4442NL(x)			{}
#endif

#ifdef DEBUG_USER
 #define D_USER(x)				D(x)
 #define D_USERNL(x)			DNL(x)
#else
 #define D_USER(x)				{}
 #define D_USERNL(x)			{}
#endif

#ifdef DEBUG_SUPER
 #define D_SUPER(x)				D(x)
 #define D_SUPERNL(x)			DNL(x)
#else
 #define D_SUPER(x)				{}
 #define D_SUPERNL(x)			{}
#endif

#endif /* DEBUG_H_ */
