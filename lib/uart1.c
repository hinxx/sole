/*
 * uart1.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "uart1.h"


/* UART1 initialization
 *  - baud (use UART_BAUD macro in uart.h)
 *  - mode (see modes in uart.h)
 *  - fmode (see fmodes in uart.h)
 *
 * Example: uart1_init(B115200, UART_8N1, UART_FIFO_14);
 */
void uart1_init(uint16_t baud, uint8_t mode, uint8_t fmode) {
	/* setup Pin Function Select Register (Pin Connect Block) */
	PCB_PINSEL0 = (PCB_PINSEL0 & ~UART1_PINMASK) | UART1_PINSEL;

	/* disable all interrupts */
	UART1_IER = 0x00;
	/* clear interrupt ID register */
	UART1_IIR = 0x00;
	/* clear line status register */
	UART1_LSR = 0x00;

	/* set the baud rate - DLAB must be set to access DLL/DLM */

	/* set divisor latches (DLAB) */
	UART1_LCR = ULCR_DLAB_EN;
	/* set for baud low byte */
	UART1_DLL = (uint8_t) baud;
	/* set for baud high byte */
	UART1_DLM = (uint8_t) (baud >> 8);

	/* set the number of characters and data its, parity, stop bits */
	/* clear divisor latches (DLAB) */
	UART1_LCR = (mode & (~ULCR_DLAB_EN));

	/* setup FIFO Control Register (fifo-enabled + xx trig) */
	UART1_FCR = fmode;
}

int uart1_putc(int ch) {
	/* wait for TX buffer to empty */
	while (!(UART1_LSR & ULSR_THRE)) {
		/* also either WDOG() or swap() */
		continue;
	}

	/* put char to Transmit Holding Register */
	UART1_THR = (uint8_t) ch;

	/* return char ("stdio-compatible"?) */
	return (uint8_t) ch;
}

const char *uart1_puts(const char *string) {
	char ch;

	while ((ch = *string)) {
		if (uart1_putc(ch) < 0)
			break;
		string++;
	}

	return string;
}

int uart1_tx_empty(void) {
	return (UART1_LSR & (ULSR_THRE | ULSR_TEMT)) == (ULSR_THRE | ULSR_TEMT);
}

void uart1_tx_flush(void) {
	/* clear the TX fifo */
	UART1_FCR |= UFCR_TX_RST;
}

/* Returns: character on success, -1 if no character is available */
int uart1_getc(void) {
	/* check if character is available */
	if (UART1_LSR & ULSR_RDR) {
		/* return character */
		return UART1_RBR;
	}

	return -1;
}

void uart1_set_handler(unsigned long *ptr) {

	/* enable RDA interrupt */
	UART1_IER = UIER_ERBFI;

	/* set up VIC */

	/* set priority 4 for UART 1 */
	VICVectCntl4 = 0x00000020 | UART1_IntEnBit;
	/* assign ISR routine */
	VICVectAddr4 = (unsigned long) ptr;
	/* enable UART1 VIC */
	VICIntEnable = (1 << UART1_IntEnBit);
}

#if 0
/*
 * UART1 receive data IRQ can utilized by using a bit more code
 */

/* UART1 buffers */
/* used to buffer data arriving in IRQ routine */
#define RX_BUFFER_SIZE1 100
volatile unsigned char rx_buffer1[RX_BUFFER_SIZE1];
volatile unsigned char rx_wr_index1 = 0, rx_bytes1 = 0;

/* used to buffer data before sending over TX */
#define TX_BUFFER_SIZE1 100
unsigned char tx_buffer1[TX_BUFFER_SIZE1];
unsigned char tx_wr_index1 = 0, tx_counter1 = 0;


/*
 * Example of UART IRQ routine
 */
void __attribute__ ((interrupt("IRQ"))) uart1_default_int(void) {
	unsigned char data;

	/* new byte, latch data */
	data = UART1_RBR;

	if ((UART1_IIR & 0xF) == 0x4) {
		/* byte okay */
		if (rx_bytes1 < RX_BUFFER_SIZE1) {
			/* space in buffer is available */
			rx_bytes1++;
			/* store in buffer */
			rx_buffer1[rx_wr_index1] = data;

			/* reset write index */
			if (++rx_wr_index1 >= RX_BUFFER_SIZE1) {
				rx_wr_index1 = 0;
			}
		}
	}

	/* update priority hardware */
	VICVectAddr = 0;
}

/*
 * Example of buffered data sending
 */
unsigned char uart1_tx_char(unsigned char inchar) {
	int tempc;

	if (tx_counter1 < TX_BUFFER_SIZE1) {
		tempc = tx_wr_index1 + tx_counter1;
		if (tempc >= TX_BUFFER_SIZE1) {
			tx_buffer1[tempc - TX_BUFFER_SIZE1] = inchar;
		} else {
			tx_buffer1[tempc] = inchar;
		}
		/* new byte added */
		tx_counter1++;

		/* wrote successfully */
		return 0;
	} else {
		/* buffer full */
		return 1;
	}
}

void uart1_tx_send() {
	int tempc;

	while (tx_counter1 > 0) {
		//if ((UART1_LSR & ULSR_THRE) != 0) {
		if (uart1_tx_empty()) {
			/* send byte */
			UART1_THR = tx_buffer1[tx_wr_index1++];
			/* one less byte to send */
			tx_counter1--;

			if (tx_wr_index1 >= TX_BUFFER_SIZE1) {
				/* reset write index */
				tx_wr_index1 = 0;
			}
		}
	}
}
#endif
