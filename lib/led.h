/*
 * led.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef LED_H_
#define LED_H_

#define LED_PIN			12
#define LED_MASK		(1 << LED_PIN)

void led_init(void);
void led_on(void);
void led_off(void);
void led_toggle(void);

#endif /* LED_H_ */
