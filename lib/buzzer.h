/*
 * buzzer.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef BUZZER_H_
#define BUZZER_H_

#define BUZZER_PIN			8
#define BUZZER				(1 << BUZZER_PIN)

void buzzer_init(void);
void buzzer_buzz(int repeat, int delay_on, int delay_off);
void buzzer_beep(void);
void buzzer_long_beep(void);
void buzzer_short_beep(void);

#endif /* BUZZER_H_ */
