/*
 * timer.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "config.h"
#include "timer.h"

void timer0_init(void) {
	/* set up Timer 0 */

	/* clear the timer count */
	T0_TC = 0;
	/* prescale divide by 2 */
	T0_PR = 1;
	/* divide by 14400 to get 2048 ints per second */
	T0_MR0 = 14400;
	/* interrupt on match, reset on match */
	T0_MCR = 0x03;

	/* stop the timer for the time being */
	T0_TCR = 0x00;

}

void timer0_set_handler(unsigned long *ptr) {
	/* start the timer */
	T0_TCR = 0x01;

	/* set up VIC */

	/* set priority 3 for Timer 0 */
	VICVectCntl3 = 0x00000024;
	/* assign ISR routine */
	VICVectAddr3 = (unsigned long)ptr;
	/* enable Timer 0 VIC */
	VICIntEnable = (1 << T0_IntEnBit);

}
