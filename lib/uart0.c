/*
 * uart0.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "uart0.h"

/* UART0 initialization
 *  - baud (use UART_BAUD macro in uart.h)
 *  - mode (see modes in uart.h)
 *  - fmode (see fmodes in uart.h)
 *
 * Example: uart0_init(B9600, UART_8N1, UART_FIFO_8);
 */
void uart0_init(uint16_t baud, uint8_t mode, uint8_t fmode) {
	/* setup Pin Function Select Register (Pin Connect Block) */
	PCB_PINSEL0 = (PCB_PINSEL0 & ~UART0_PINMASK) | UART0_PINSEL;

	/* disable all interrupts */
	UART0_IER = 0x00;
	/* clear interrupt ID register */
	UART0_IIR = 0x00;
	/* clear line status register */
	UART0_LSR = 0x00;

	/* set the baud rate - DLAB must be set to access DLL/DLM */

	/* set divisor latches (DLAB) */
	UART0_LCR = ULCR_DLAB_EN;
	/* set for baud low byte */
	UART0_DLL = (uint8_t) baud;
	/* set for baud high byte */
	UART0_DLM = (uint8_t) (baud >> 8);

	/* set the number of characters and data its, parity, stop bits */
	/* clear divisor latches (DLAB) */
	UART0_LCR = (mode & (~ULCR_DLAB_EN));

	/* setup FIFO Control Register (fifo-enabled + xx trig) */
	UART0_FCR = fmode;
}

int uart0_putc(int ch) {
	/* wait for TX buffer to empty */
	while (!(UART0_LSR & ULSR_THRE)) {
		/* also either WDOG() or swap() */
		continue;
	}

	/* put char to Transmit Holding Register */
	UART0_THR = (uint8_t) ch;

	/* return char ("stdio-compatible"?) */
	return (uint8_t) ch;
}

const char *uart0_puts(const char *string) {
	char ch;

	while ((ch = *string)) {
		if (uart0_putc(ch) < 0)
			break;
		string++;
	}

	return string;
}

int uart0_tx_empty(void) {
	return (UART0_LSR & (ULSR_THRE | ULSR_TEMT)) == (ULSR_THRE | ULSR_TEMT);
}

void uart0_tx_flush(void) {
	/* clear the TX fifo */
	UART0_FCR |= UFCR_TX_RST;
}

/* Returns: character on success, -1 if no character is available */
int uart0_getc(void) {
	/* check if character is available */
	if (UART0_LSR & ULSR_RDR) {
		/* return character */
		return UART0_RBR;
	}

	return -1;
}

void uart0_set_handler(unsigned long *ptr) {

	/* enable RDA interrupt */
	UART0_IER = UIER_ERBFI;

	/* set up VIC */

	/* set priority 5 for UART 0 */
	VICVectCntl5 = 0x00000020 | UART0_IntEnBit;
	/* assign ISR routine */
	VICVectAddr5 = (unsigned long) ptr;
	/* enable UART0 VIC */
	VICIntEnable = (1 << UART0_IntEnBit);
}

#if 0
/*
 * UART0 receive data IRQ can utilized by using a bit more code
 */

/* UART0 buffers */
/* used to buffer data arriving in IRQ routine */
#define RX_BUFFER_SIZE0 100
volatile unsigned char rx_buffer0[RX_BUFFER_SIZE0];
volatile unsigned char rx_wr_index0 = 0, rx_bytes0 = 0;

/* used to buffer data before sending over TX */
#define TX_BUFFER_SIZE0 100
unsigned char tx_buffer0[TX_BUFFER_SIZE0];
unsigned char tx_wr_index0 = 0, tx_counter0 = 0;

/*
 * Example of UART IRQ routine
 */
void __attribute__ ((interrupt("IRQ"))) uart0_default_int(void) {
	unsigned char data;

	/* new byte, latch data */
	data = UART0_RBR;

	if ((UART0_IIR & 0xF) == 0x4) {
		/* byte okay */
		if (rx_bytes0 < RX_BUFFER_SIZE0) {
			/* space in buffer is available */
			rx_bytes0++;
			/* store in buffer */
			rx_buffer0[rx_wr_index0] = data;

			/* reset write index */
			if (++rx_wr_index0 >= RX_BUFFER_SIZE0) {
				rx_wr_index0 = 0;
			}
		}
	}

	/* update priority hardware */
	VICVectAddr = 0;
}

/*
 * Example of buffered data sending
 */
unsigned char uart0_tx_char(unsigned char inchar) {
	int tempc;

	if (tx_counter0 < TX_BUFFER_SIZE0) {
		tempc = tx_wr_index0 + tx_counter0;
		if (tempc >= TX_BUFFER_SIZE0) {
			tx_buffer0[tempc - TX_BUFFER_SIZE0] = inchar;
		} else {
			tx_buffer0[tempc] = inchar;
		}
		/* new byte added */
		tx_counter0++;

		/* wrote successfully */
		return 0;
	} else {
		/* buffer full */
		return 1;
	}
}

void uart0_tx_send() {
	int tempc;

	while (tx_counter0 > 0) {
		//if ((UART0_LSR & ULSR_THRE) != 0) {
		if (uart0_tx_empty()) {
			/* send byte */
			UART0_THR = tx_buffer0[tx_wr_index0++];
			/* one less byte to send */
			tx_counter0--;

			if (tx_wr_index0 >= TX_BUFFER_SIZE0) {
				/* reset write index */
				tx_wr_index0 = 0;
			}
		}
	}
}
#endif
