/*
 * lcd.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef LCD_H_
#define LCD_H_

#define LCD_D4_PIN			4
#define LCD_D5_PIN			5
#define LCD_D6_PIN			6
#define LCD_D7_PIN			7
#define LCD_EN_PIN			22
#define LCD_RS_PIN			23
#define LCD_RW_PIN			24

#define LCD_D4				(1 << LCD_D4_PIN)
#define LCD_D5				(1 << LCD_D5_PIN)
#define LCD_D6				(1 << LCD_D6_PIN)
#define LCD_D7				(1 << LCD_D7_PIN)
#define LCD_EN				(1 << LCD_EN_PIN)
#define LCD_RS				(1 << LCD_RS_PIN)
#define LCD_RW				(1 << LCD_RW_PIN)
#define LCD_DATA			(LCD_D4|LCD_D5|LCD_D6|LCD_D7)
#define LCD_IOALL			(LCD_D4|LCD_D5|LCD_D6|LCD_D7|LCD_EN|LCD_RS|LCD_RW)

void lcd_init(void);
void lcd_wait(void);
void lcd_out_data4(unsigned char val);
void lcd_write_nibbles(unsigned char val);
void lcd_write_control(unsigned char val);
void lcd_putchar(unsigned char val);
void lcd_print(const char *str);
void lcd_print_center(const char *str);
void lcd_print_both(const char *line1, const char *line2);
void lcd_print_line1(const char *s);
void lcd_print_line2(const char *s);

/* internal I/O functions */
#define lcd_rs_set()		GPIO_IOSET |= LCD_RS
#define lcd_rs_clr()		GPIO_IOCLR |= LCD_RS
#define lcd_en_set()		GPIO_IOSET |= LCD_EN
#define lcd_en_clr()		GPIO_IOCLR |= LCD_EN
#define lcd_rw_set()		GPIO_IOSET |= LCD_RW
#define lcd_rw_clr()		GPIO_IOCLR |= LCD_RW

/* LCD commands */
#define lcd_clear()			lcd_write_control(0x01)
#define lcd_cursor_home()	lcd_write_control(0x02)

#define lcd_display_on()	lcd_write_control(0x0E)
#define lcd_display_off()	lcd_write_control(0x08)

#define lcd_cursor_blink()	lcd_write_control(0x0F)
#define lcd_cursor_on()		lcd_write_control(0x0E)
#define lcd_cursor_off()	lcd_write_control(0x0C)

#define lcd_cursor_left()	lcd_write_control(0x10)
#define lcd_cursor_right()	lcd_write_control(0x14)
#define lcd_display_sleft()	lcd_write_control(0x18)
#define lcd_display_sright()lcd_write_control(0x1C)

#endif /* LCD_H_ */
