/*
 * utils.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef UTILS_H_
#define UTILS_H_

#include <stdint.h>

void *memset(void *s, int c, uint32_t n);
void *memcpy(void *dest, const void *src, uint32_t n);
int strlen(char *s);
char *strcpy(char *dest, const char *src);
void itoa(int n, char s[]);
void sprintf_hex(char *dest, char *src, int len);
void sprintf_i(char *dest, int i);
void sprintf_si(char *dest, char *s, int i);
void sprintf_is(char *dest, int i, char *s);
void sprintf_sis(char *dest, char *s1, int i, char *s2);
void sprintf_isis(char *dest, int i1, char *s1, int i2, char *s2);


#endif /* UTILS_H_ */
