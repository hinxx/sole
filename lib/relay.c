/*
 * relay.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "relay.h"

/*
 * Connect COM (common) and NO (normally open) pins of the relay, this way
 * when the relay is activated, the switch will be closed!
 */
void relay_init(void) {
	/* relay does not conduct */
	GPIO_IOCLR |= RELAY_MASK;
	/* relay pin is output */
	GPIO_IODIR |= RELAY_MASK;
}

void relay_open(void) {
	/* relay does not conduct */
	GPIO_IOCLR |= RELAY_MASK;
}

void relay_close(void) {
	/* relay conducts */
	GPIO_IOSET |= RELAY_MASK;
}
