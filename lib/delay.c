/*
 * delay.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "delay.h"


void delay_us(int count) {
	int i;
	for (i = 0; i < count; i++) {
		__asm__ __volatile__ ("nop");
	}
}

void delay_ms(int count) {
	int i;
	count *= 2600;
	for (i = 0; i < count; i++) {
		__asm__ __volatile__ ("nop");
	}
}

void delay_s(int count) {
	int i;
	for (i = 0; i < count; i++) {
		delay_ms(1000);
	}
}
