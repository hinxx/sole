/*
 * timer.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef TIMER_H_
#define TIMER_H_

#define T0_IntEnBit		4

void timer0_init(void);
void timer0_set_handler(unsigned long *ptr);

#endif /* TIMER_H_ */
