/*
 * events.c
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#include "lpc210x_gnuarm.h"
#include "config.h"
#include "events.h"

#define MAX_EVENTS			100

static int events[MAX_EVENTS];
static int event_rd_index = 0;
static int event_wr_index = 0;
static int event_count = 0;
static volatile unsigned int event_mx = 0;

static void mx_lock(void) {
	do {
	} while (event_mx != 0);
	event_mx = 1;
}

static void mx_unlock(void) {
	event_mx = 0;
}

void events_reset(void) {
	mx_lock();
	event_count = 0;
	event_wr_index = 0;
	event_rd_index = 0;
	mx_unlock();
}

void events_set_next(int ev) {
	mx_lock();

	if (event_count >= MAX_EVENTS) {
		mx_unlock();
		return;
	}

	events[event_wr_index] = ev;

	event_wr_index++;
	if (event_wr_index >= MAX_EVENTS) {
		event_wr_index = 0;
	}
	event_count++;

	mx_unlock();
}

int events_get_next(void) {
	mx_lock();

	if (event_count == 0) {
		mx_unlock();
		return EV_IDLE;
	}

	int ev = events[event_rd_index];

	event_rd_index++;
	if (event_rd_index >= MAX_EVENTS) {
		event_rd_index = 0;
	}
	event_count--;

	mx_unlock();
	return ev;
}
