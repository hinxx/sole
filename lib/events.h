/*
 * events.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: sole
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/sole
 */


#ifndef EVENTS_H_
#define EVENTS_H_

typedef struct {
	int st;
	int ev;
	int (*fn)(void);
} state_transition;

/* default events */
#define EV_ANY				-1
#define EV_IDLE				0

void events_reset(void);
int events_get_next(void);
void events_set_next(int ev);

#endif /* EVENTS_H_ */
